﻿using DailyBasket.Franchisee.Core.Enum;
using DailyBasket.Franchisee.Core.Model.DropDownList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;

namespace DailyBasket.Franchisee.Helper
{
    public class DropdownlistHelper
    {
        public static SelectList GetIntStringDropDownList(DataTable dataTable)
        {
            List<IntStringDdlModel> intStringModelList = GenerateIntStringDropDownList(dataTable);
            if (intStringModelList != null)
                return new SelectList(intStringModelList, "Id", "Name");
            return null;
        }


        public static SelectList GetLongStringDropDownList(DataTable dataTable)
        {
            List<LongStringDdlModel> longStringModelList = GenerateLongStringDropDownListWithDdlBinding(dataTable);
            if (longStringModelList != null)
                return new SelectList(longStringModelList, "Id", "Name");
            return null;
        }

        public static SelectList GetLongStringDDList(DataTable dataTable)
        {
            List<LongStringDdlModel> longStringModelList = GenerateLongStringDropDownList(dataTable);
            if (longStringModelList != null)
                return new SelectList(longStringModelList, "Id", "Name");
            return null;
        }

        public static List<IntStringDdlModel> GetIntStringModelDropDownList(DataTable dataTable)
        {
            return GenerateIntStringDropDownList(dataTable);
        }


        public static List<LongStringDdlModel> GetLongStringModelDropDownList(DataTable dataTable)
        {
            return GenerateLongStringDropDownList(dataTable);
        }

        
        

        // Pass DataTable 
        // DataTable's row's First Parameter must be Integer type and Second must be string
        private static List<IntStringDdlModel> GenerateIntStringDropDownList(DataTable dataTable)
        {
            List<IntStringDdlModel> intStringModelList = new List<IntStringDdlModel>();
            if (dataTable != null)
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    intStringModelList.Add(new IntStringDdlModel { Name = row[1].ToString(), Id = Convert.ToUInt32(row[0].ToString()) });
                }
                return intStringModelList;
            }
            return null;
        }

        // Pass DataTable 
        // DataTable's row's First Parameter must be long type and Second must be string
        private static List<LongStringDdlModel> GenerateLongStringDropDownList(DataTable dataTable)
        {
            List<LongStringDdlModel> longStringModelList = new List<LongStringDdlModel>();
            if (dataTable != null)
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    longStringModelList.Add(new LongStringDdlModel { Name = row[1].ToString(), Id = Convert.ToUInt32(row[0].ToString()) });
                }
                return longStringModelList;
            }
            return null;
        }

        private static List<LongStringDdlModel> GenerateLongStringDropDownListWithDdlBinding(DataTable dataTable)
        {
            List<LongStringDdlModel> longStringModelList = new List<LongStringDdlModel>();
            if (dataTable != null)
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    longStringModelList.Add(new LongStringDdlModel { Name = row[1].ToString() +  "-" +  "(Mrp : " + row[2].ToString() + ")", Id = Convert.ToUInt32(row[0].ToString()) });
                }
                return longStringModelList;
            }
            return null;
        }
    }
}