﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DailyBasket.Franchisee.Helper
{
    public class LoyaltyPointHelper
    {
        public static float GetCustomerLoyaltyPoints(DataTable dataTable)
        {
            if(dataTable != null && dataTable.Rows.Count > 0)
            {
                if (dataTable.Rows[0]["referred_referral_code"].ToString() != null)
                {
                    if (Convert.ToUInt16(dataTable.Rows[0]["order_count"].ToString()) == 1)
                    {
                        return (Convert.ToSingle(dataTable.Rows[0]["total_order_amount"]) * Convert.ToSingle(dataTable.Rows[0]["first_order_percent"]) / 100);
                    }
                    else
                    {
                        return (Convert.ToSingle(dataTable.Rows[0]["total_order_amount"]) * Convert.ToSingle(dataTable.Rows[0]["second_and_next_order_percent"]) / 100);
                    }
                }
            }
            return 0;
        }
    }
}