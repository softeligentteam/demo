﻿//using DailyBasket.Warehouse.Areas.Purchase.BLL;
using DailyBasket.Franchisee.Core.Model.Login;
using DailyBasket.Franchisee.Core.Model.Purchase;
//using DailyBasket.Warehouse.Core.Model.Purchase;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace DailyBasket.Franchisee.Helper
{
    public class SessionManager
    {
        public static bool SetPurchaseOrderCookie(string cookieName, Dictionary<string, string> POC)
        {
            
            HttpCookie poCookie = new HttpCookie(cookieName);
            //[POC.Keys]  = POC.Value.tos;
            if (POC.Count > 0)
            {
                    foreach (KeyValuePair<string, string> val in POC)
                    {
                       //if(GetHttpRequest().Cookies.AllKeys.Contains(val.Key) != true)
                       // {
                            poCookie[val.Key] = val.Value.ToString();
                        //}
                    }
                    poCookie.Expires = DateTime.Now.AddDays(1);
                    HttpContext.Current.Response.Cookies.Add(poCookie);
                    return true;
            }
            return false;
        }

        public static bool SetPurchaseOrderCookieId(ulong poId)
        {
            HttpCookie poCookie = new HttpCookie("PurchaseOrderCookie");
            if(poId != 0)
            {
                poCookie["poId"] = poId.ToString();
                poCookie.Expires = DateTime.Now.AddDays(1);
                HttpContext.Current.Response.Cookies.Add(poCookie);
                return true;
            }
            return false;
        }
        //Creating Http request
        public static HttpRequest GetHttpRequest()
        {
            return HttpContext.Current.Request;
        }
        public static void SetFormsAuthentication(SessionUserModel sessionUserModel)
        {
            // Autherization
            FormsAuthentication.SetAuthCookie(sessionUserModel.UserType+sessionUserModel.FranchiseeId, false);

            var userData = new Dictionary<string, string>();
            userData.Add("FcUserId", sessionUserModel.FranchiseeId.ToString());
            userData.Add("FcUserName", sessionUserModel.UserName);
            userData.Add("FcFranchiseeName", sessionUserModel.FranchiseeName);
            userData.Add("CRole", sessionUserModel.UserType);
            string userDataString = JsonConvert.SerializeObject(userData);
            var authTicket = new FormsAuthenticationTicket(1, "UserDataCookie", DateTime.Now, DateTime.Now.AddMinutes(30), false, userDataString);
            string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
            var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            //HttpContext.Response.Cookies.Add(authCookie);
            HttpContext.Current.Response.Cookies.Add(authCookie);
        }

        public static bool UnsetPurchaseOrderCookie()
        {
            if (HttpContext.Current.Request.Cookies["PurchaseOrderCookie"] != null)
            {
                HttpCookie poCookie = HttpContext.Current.Request.Cookies["PurchaseOrderCookie"];
                poCookie.Value = null;
                poCookie.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies.Add(poCookie);
                return true;
            }
            return false;
        }
        public static void UnsetFormsAuthentication()
        {
            if(HttpContext.Current.Request != null)
            {
                /*FormsAuthentication.SignOut();
                //HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
                //HttpContext.Current.Session.Abandon();

                // clear authentication cookie
                HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                authCookie.Expires = DateTime.Now.AddYears(-1);
                HttpContext.Current.Response.Cookies.Add(authCookie);
                
                HttpContext.Current.Request.Cookies.Clear();
                HttpContext.Current.Response.Cookies.Clear();*/

                FormsAuthentication.SignOut();

                // Drop all the information held in the session
                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session.Abandon();

                // clear authentication cookie
                HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                cookie1.Expires = DateTime.Now.AddYears(-1);
                HttpContext.Current.Response.Cookies.Add(cookie1);

                // clear session cookie
                HttpCookie cookie2 = new HttpCookie("ASP.NET_SessionId", "");
                cookie2.Expires = DateTime.Now.AddYears(-1);
                HttpContext.Current.Response.Cookies.Add(cookie2);
            }
        }

        public static ulong CurrentUserId
        {
            get
            {
                ulong userId = 0;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    FormsIdentity formsIdentity = HttpContext.Current.User.Identity as FormsIdentity;
                    FormsAuthenticationTicket authTicket = formsIdentity.Ticket;
                    var userData = JsonConvert.DeserializeObject<Dictionary<string, string>>(authTicket.UserData);
                    userId = (ulong)Convert.ToInt64(userData["FcUserId"].ToString());
                }

                return userId;
            }
        }

        public static string CurrentUserName
        {
            get
            {
                string userName = string.Empty;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    FormsIdentity formsIdentity = HttpContext.Current.User.Identity as FormsIdentity;
                    FormsAuthenticationTicket authTicket = formsIdentity.Ticket;
                    var userData = JsonConvert.DeserializeObject<Dictionary<string, string>>(authTicket.UserData);
                    userName = userData["FcUserName"].ToString();
                }

                return userName;
            }
        }

        public static string CurrentFranchiseeName
        {
            get
            {
                string franchiseeName = string.Empty;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    FormsIdentity formsIdentity = HttpContext.Current.User.Identity as FormsIdentity;
                    FormsAuthenticationTicket authTicket = formsIdentity.Ticket;
                    var userData = JsonConvert.DeserializeObject<Dictionary<string, string>>(authTicket.UserData);
                    franchiseeName = userData["FcFranchiseeName"].ToString();
                }

                return franchiseeName;
            }
        }

        public static string CurrentUserType
        {
            get
            {
                string userType = string.Empty;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    FormsIdentity formsIdentity = HttpContext.Current.User.Identity as FormsIdentity;
                    FormsAuthenticationTicket authTicket = formsIdentity.Ticket;
                    var userData = JsonConvert.DeserializeObject<Dictionary<string, string>>(authTicket.UserData);
                    userType = userData["CRole"].ToString();
                }

                return userType;
            }
        }
    }
}