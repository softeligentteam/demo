﻿using DailyBasket.Franchisee.Core.Enum;
using System;
using System.Linq;
using System.Web.Mvc;

namespace DailyBasket.Franchisee.Helper
{
    public class EnumDropdownlistHelper
    {
        public static SelectList GetEnumQuantityTypeDdlList()
        {
            return new SelectList(Enum.GetValues(typeof(QuantityType)));
        }

        public static SelectList GetEnumGenderDdlList()
        {
            return new SelectList(Enum.GetValues(typeof(Gender)));
        }

        //public static SelectList GetEnumProductDisplayIndexDdlList()
        //{
        //    return new SelectList(Enum.GetValues(typeof(ProductDisplayIndex)));
        //}

        //public static SelectList GetEnumProductDisplayIndexIdNameDdlList()
        //{
        //    var list = from ProductDisplayIndex prodcutIndex in Enum.GetValues(typeof(ProductDisplayIndex))
        //                       select new
        //                       {
        //                           Id = (int)prodcutIndex,
        //                           Name = prodcutIndex.ToString()
        //                       };
        //    return new SelectList(list, "Id", "Name");
        //}

        //public static SelectList GetEnumPurchaseReportList()
        //{
        //    return new SelectList(Enum.GetValues(typeof(PurchaseReportEnum)));
        //}
        //public static SelectList GetEnumSalesReportList()
        //{
        //    return new SelectList(Enum.GetValues(typeof(SalesReports)));
        //}
        //public static SelectList GetEnumStockReportList()
        //{
        //    return new SelectList(Enum.GetValues(typeof(StockReport)));
        //}
        public static SelectList GetEnumCashOnDeliveryPMList()
        {
            return new SelectList(Enum.GetValues(typeof(CashOnDeliveryPaymentMode)));
        }
        public static SelectList GetEnumPurchaseReportList()
        {
            return new SelectList(Enum.GetValues(typeof(PurchaseReportEnum)));
        }
        public static SelectList GetEnumStockReportList()
        {
            return new SelectList(Enum.GetValues(typeof(StockReport)));
        }
        public static SelectList GetEnumSalesReportList()
        {
            return new SelectList(Enum.GetValues(typeof(SalesReports)));
        }
    }
}