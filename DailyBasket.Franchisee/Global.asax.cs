﻿using DailyBasket.Franchisee.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Optimization;
using System.Web.Security;
using Newtonsoft.Json;
using DailyBasket.Franchisee.Helper;
using DailyBasket.Franchisee;

namespace DailyBasket.Franchisee
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            HttpContext.Current.Application["ImageAccessPath"] = "http://static.dailybaskets.co.in/";
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                if (authTicket != null && !authTicket.Expired)
                {
                    if (HttpContext.Current.Request.IsAuthenticated)
                    {
                        Dictionary<string, string> userData = JsonConvert.DeserializeObject<Dictionary<string, string>>(authTicket.UserData);
                        //var roles = authTicket.UserData.Split(',');
                        string[] roles = { userData["CRole"].ToString() };
                        HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(new FormsIdentity(authTicket), roles);
                    }
                }
            }
        }

        /*protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            Server.ClearError();
            Response.Redirect("ErrorHandler");
        }*/
    }
}
