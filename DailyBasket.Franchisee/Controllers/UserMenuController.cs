﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Warehouse.Controllers
{
    [ChildActionOnly]
    public class UserMenuController : Controller
    {
        // GET: UserMenu
        public ActionResult Master()
        {
            return View("UserMenu/_Master");
        }

        
        public ActionResult Purchase_Manager()
        {
            return View("UserMenu/_PurchaseManager");
        }

        
        public ActionResult Sale_Manager()
        {
            return View("UserMenu/_SaleManager");
        }

        
        public ActionResult Account_Manger()
        {
            return View("UserMenu/_AccountManager");
        }

        
        public ActionResult Delivery()
        {
            return View("UserMenu/_Delivery");
        }

        
        public ActionResult Partner()
        {
            return View("UserMenu/_Partner");
        }

        
        public ActionResult Inverstor()
        {
            return View("UserMenu/_Inverstor");
        }
    }
}