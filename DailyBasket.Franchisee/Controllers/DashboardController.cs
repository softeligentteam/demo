﻿using DailyBasket.Franchisee.BLL;
//using DailyBasket.Franchisee.Core.Model.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Warehouse.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        DashboardBLL dashboardBLL;
        public DashboardController()
        {
            dashboardBLL = new DashboardBLL();
        }
        // GET: Dashboard
        public ActionResult Index()
        {
            return View("Dashboard");
        }
    }
}