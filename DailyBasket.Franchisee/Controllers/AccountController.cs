﻿using DailyBasket.Franchisee.BLL;
using DailyBasket.Franchisee.Core.Model.Login;
using DailyBasket.Franchisee.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Services;
using System.Web.SessionState;
using DailyBasket.Franchisee.Helper;

namespace DailyBasket.Franchisee.Controllers
{
    public class AccountController : Controller
    {
        AccountBLL accountBLL;
        public AccountController()
        {
            accountBLL = new AccountBLL();
        }

        // GET: Login
        public ActionResult Index()
        {
            return RedirectToAction("LoginPage");
        }

        public ActionResult LoginPage()
        {
            SessionManager.UnsetFormsAuthentication();
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoginPage(LoginModel loginModel, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                SessionUserModel userModel = accountBLL.Authenticate(loginModel);

                if (userModel != null)
                {
                    SessionManager.SetFormsAuthentication(userModel);
                    // Redirect to last visited Url
                    if(returnUrl != null)
                        if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/") && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                            return Redirect(returnUrl);
                    return RedirectToAction("Index", "Dashboard");
                }
                else
                {
                    TempData["Message"] = "Invalid Mobile or Password";
                    return RedirectToAction("LoginPage");
                }
            }
            else
            {
                return View(loginModel);
            }
        }

        [HttpGet]
        public ActionResult Logout()
        {
            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            SessionManager.UnsetFormsAuthentication();
            return RedirectToAction("LoginPage");
        }

        // Clear All Cookie on closing browser
        [WebMethod]
        public JsonResult ClearAllBrowserCookie()
        {
            SessionManager.UnsetFormsAuthentication();
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}