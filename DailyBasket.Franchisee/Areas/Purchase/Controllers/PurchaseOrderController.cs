﻿
using DailyBasket.Franchisee.Areas.Purchase.BLL;
using DailyBasket.Franchisee.Core.Enum;
using DailyBasket.Franchisee.Core.Interface.BLL.Purchase;
using DailyBasket.Franchisee.Core.Model.Purchase;
using DailyBasket.Franchisee.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Franchisee.Areas.Purchase.Controllers
{
    [Authorize]
    public class PurchaseOrderController : Controller
    {
        // private IPurchaseProduct iPurchaseProduct = new PurchaseProductBLL();
        IPurchaseProduct iPurchaseProductBLL;
        public PurchaseOrderController()
        {
            iPurchaseProductBLL = new PurchaseProductBLL();
        }

    

        private void BindPurchaseProductViewDdl()
        {
            ViewBag.ProductDdlList = new PurchaseProductBLL().GetActiveProductListDdl();
        }

   
        //code for add purchase order

        [HttpGet]
        public ActionResult AddPurchaseProductOrder()
        {
            ulong poId = iPurchaseProductBLL.AddPurchaseOrder(SessionManager.CurrentUserId);
            if (poId != 0)
            {
                TempData["poId"] = poId;
                return RedirectToAction("AddPurchaseProduct");
            }
            return RedirectToAction("MyPurchaseOrders");
        }

        [HttpGet]
        public ActionResult AddPurchaseProduct()
        {
            ulong poId = Convert.ToUInt32(TempData["poId"]);
            POProductModel poProductModel = new POProductModel();
            if (poId != 0)
            {
                poProductModel.POPProductList = iPurchaseProductBLL.GetPurchaseOrderProductListToCart(poId);
                poProductModel.Id = poId;
                BindPurchaseProductViewDdl();
                return View("AddPurchaseProductOrder", poProductModel);
            }
            return RedirectToAction("MyPurchaseOrders");  // return to myorders List
        }

        [HttpPost]
        public ActionResult AddPurchaseProduct(POProductModel pOProductModel)
        {
            if (ModelState.IsValid)
            {
                if(pOProductModel != null)
                {
                    if (iPurchaseProductBLL.AddPurchaseProductOrder(pOProductModel) != 0)
                    {
                        TempData["Message"] = "Product added successfully.";
                        TempData["CssClass"] = "text-success";
                    }
                    else
                    {
                        TempData["Message"] = "Oops error Occured, Please try again!";
                        TempData["CssClass"] = "text-danger";
                    }
                    TempData["poId"] = pOProductModel.Id;
                    return RedirectToAction("AddPurchaseProduct");
                }
            }
            return RedirectToAction("MyPurchaseOrders");    // redirect to my order list;
        }

        [HttpGet]
        public ActionResult DeletePurchaseProduct(ulong poId = 0, ulong prodId = 0)
        {
            if (poId != 0 && prodId != 0)
            {
                if(iPurchaseProductBLL.DeletePurchaseProduct(poId, prodId) != 0)
                {
                    TempData["Message"] = "Product deleted successfully.";
                    TempData["CssClass"] = "text-success";
                }
                else
                {
                    TempData["Message"] = "product not deleted successfully, Please try again!";
                    TempData["CssClass"] = "text-danger";
                }
            }
            TempData["poId"] = poId;
            return RedirectToAction("AddPurchaseProduct");
        }

        [HttpPost]
        public ActionResult AddPurchaseProductOrder(ulong poId = 0)
        {
            if (poId != 0)
            {
                if (iPurchaseProductBLL.UpdateSubmitTypeofPOP(poId) != 0)
                {
                    return RedirectToAction("MyPurchaseOrders");
                }
            }
            TempData["poId"] = poId;
            return RedirectToAction("AddPurchaseProduct");
        }

        //List of Purchase Orders

        [HttpGet]
        public ActionResult MyPurchaseOrders()
        {
            List<POProductModel> poProductModel = iPurchaseProductBLL.GetPurchaseOrders(SessionManager.CurrentUserId);
            return View(poProductModel);
        }

        [HttpGet]
        public ActionResult PurchaseOrderDetails(ulong poId = 0)
        {
            if(poId != 0)
            {
                POProductModel poProductModel = iPurchaseProductBLL.GetPurchaseProducts(poId);
                return View(poProductModel);
            }
            return View();
        }

        public ActionResult DeliveredOrders()
        {
            ViewBag.MethodName = "DeliveredOrder";
            ViewBag.PageTitle = "Delivered Franchisee Orders";
            return View("FranchiseeOrders", GetFranchiseeOrders(OrderStatus.Delivered));
        }

        public ActionResult CancelledOrders()
        {
            ViewBag.MethodName = "CancelledOrder";
            ViewBag.PageTitle = "Cancelled Franchisee Orders";
            return View("FranchiseeOrders", GetFranchiseeOrders(OrderStatus.Cancelled));
        }
        
        public ActionResult DeliveredOrder(ulong foId = 0)
        {
            ViewBag.PageTitle = "Delivered Customer Order";
            POProductModel poProductModel = GetFranchiseeOrder(OrderStatus.Delivered, foId);
            if (poProductModel == null)
                return RedirectToAction("DeliveredOrders");
            poProductModel.Status = OrderStatus.Delivered;
            return View("FranchiseeOrder", poProductModel);
        }

        public ActionResult CancelledOrder(ulong foId = 0)
        {
            ViewBag.PageTitle = "Cancelled Customer Order";

            POProductModel poProductModel = GetFranchiseeOrder(OrderStatus.Cancelled, foId);
            if (poProductModel == null)
                return RedirectToAction("CancelledOrders");
            poProductModel.Status = OrderStatus.Cancelled;
            return View("FranchiseeOrder", poProductModel);
        }
        
        private List<POProductModel> GetFranchiseeOrders(OrderStatus orderStatus)
        {
            return iPurchaseProductBLL.GetFranchiseeOrders(orderStatus, SessionManager.CurrentUserId, SubmitType.Permanent);
        }

        private POProductModel GetFranchiseeOrder(OrderStatus orderStatus, ulong coId)
        {
            return iPurchaseProductBLL.GetFranchiseeOrder(coId, SessionManager.CurrentUserId, orderStatus);
        }
    }
} 