﻿using DailyBasket.Franchisee.Core.Interface.BLL.Purchase;
using DailyBasket.Franchisee.Helper;
using DailyBasket.Franchisee.Repository.Purchase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using DailyBasket.Franchisee.Core.Model.Purchase;
using DailyBasket.Franchisee.Core.Enum;
using System.Runtime.CompilerServices;

namespace DailyBasket.Franchisee.Areas.Purchase.BLL
{
    public class PurchaseProductBLL : IPurchaseProduct
    {
        Core.Interface.DAL.Purchase.IPurchaseProduct ipurchaseProductRepository;
        public PurchaseProductBLL()
        {
            ipurchaseProductRepository = new PurchaseProductRepository();
        }

        public ulong AddPurchaseOrder(ulong fcUserId)
        {
            return ipurchaseProductRepository.AddPurchaseOrder(fcUserId);
        }

        public int AddPurchaseProductOrder(POProductModel poproductModel)
        {
            return ipurchaseProductRepository.AddPurchaseProductOrder(poproductModel);
        }

        public SelectList GetActiveProductListDdl()
        {
            return DropdownlistHelper.GetLongStringDropDownList(ipurchaseProductRepository.GetActiveProductListDdl());
        }

        public List<POProductListModel> GetPurchaseOrderProductListToCart(ulong poId)
        {
            DataTable dataTable = ipurchaseProductRepository.GetPurchaseOrderProductListToCart(poId);
            if (dataTable != null)
            {
                return BindPurchaseOrderProduct(dataTable);
            }
            return null;
        }

        private List<POProductListModel> BindPurchaseOrderProduct(DataTable dataTable)
        {
            List<POProductListModel> poProductList = new List<POProductListModel>();
            if (dataTable.Rows.Count > 0)
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    POProductListModel poProductLM = new POProductListModel();
                    poProductLM.PoId = Convert.ToUInt64(row["fc_po_id"]);
                    poProductLM.ProdId = Convert.ToUInt64(row["prod_id"]);
                    poProductLM.ProdName = row["prodname"].ToString();
                    poProductLM.Quantity = Convert.ToSingle(row["quantity"]);
                    poProductList.Add(poProductLM);
                }
                return poProductList;
            }
            return null;
        }

        public int UpdateSubmitTypeofPOP(ulong poId)
        {
            return ipurchaseProductRepository.UpdateSubmitTypeofPOP(poId);
        }

        public int DeletePurchaseProduct(ulong poId, ulong prodId)
        {
            if (poId != 0 && prodId != 0)
                return ipurchaseProductRepository.DeletePurchaseProduct(poId, prodId);
            return 0;
        }

        public List<POProductModel> GetPurchaseOrders(ulong cUserId)
        {
            DataTable dataTable = ipurchaseProductRepository.GetPurchaseOrders(cUserId);
            if (dataTable.Rows.Count > 0 && dataTable != null)
            {
                return BindPurchaseOrders(dataTable);
            }
            return null;
        }

        private List<POProductModel> BindPurchaseOrders(DataTable dataTable)
        {
            List<POProductModel> pOrdersList = new List<POProductModel>();
            if (dataTable.Rows.Count > 0)
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    POProductModel pOrderLM = new POProductModel();
                    pOrderLM.BillNo = Convert.ToUInt64(row["fc_po_id"]);
                    pOrderLM.VendorName = row["vendor_name"].ToString();
                    pOrderLM.OrderDate = row["order_date"].ToString();
                    pOrdersList.Add(pOrderLM);
                }
                return pOrdersList;
            }
            return null;
        }

        public dynamic GetPurchaseProducts(ulong poId)
        {
            DataSet dataSet = ipurchaseProductRepository.GetPurchaseProducts(poId);

            if (dataSet.Tables[0].Rows.Count > 0 && dataSet.Tables[0] != null)
            {
                return BindPurchaseOrderWithProducts(dataSet);

            }
            return null;
        }

        private dynamic BindPurchaseOrderWithProducts(DataSet dataSet)
        {
            DataTable dataTable = dataSet.Tables[0];
            POProductModel poProductModel = new POProductModel();
            poProductModel.BillNo = Convert.ToUInt64(dataTable.Rows[0]["fc_po_id"]);
            poProductModel.VendorName = dataTable.Rows[0]["vendor_name"].ToString();
            poProductModel.OrderDate = dataTable.Rows[0]["order_date"].ToString();

            if (dataSet.Tables[1].Rows.Count > 0)
            {
                List<POProductListModel> pOrderList = new List<POProductListModel>();
                foreach (DataRow row in dataSet.Tables[1].Rows)
                {
                    POProductListModel poProductLM = new POProductListModel();
                    poProductLM.ProdName = row["prodname"].ToString();
                    poProductLM.Quantity = Convert.ToSingle(row["quantity"]);
                    pOrderList.Add(poProductLM);
                }
                poProductModel.POPProductList = pOrderList;
            }
            return poProductModel;
        }

       public dynamic GetFranchiseeOrders(OrderStatus os_Id, ulong cUserId, SubmitType submitType)
        {
            DataTable dataTable = ipurchaseProductRepository.GetFranchiseeOrders(os_Id, cUserId, submitType);
            if (dataTable.Rows.Count > 0 && dataTable != null)
            {
                return BindFranchiseePurchaseOrders(dataTable);
            }
            return null;
        }

        private List<POProductModel> BindFranchiseePurchaseOrders(DataTable dataTable)
        {
            List<POProductModel> pOrdersList = new List<POProductModel>();
            if (dataTable.Rows.Count > 0)
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    POProductModel pOrderLM = new POProductModel();
                    pOrderLM.BillNo = Convert.ToUInt64(row["fc_po_id"]);
                    pOrderLM.VendorName = row["vendor_name"].ToString();
                    pOrderLM.OrderDate = row["order_date"].ToString();
                    pOrderLM.TotalBillAmount = Convert.ToUInt64(row["total_bill_amount"]);
                    pOrdersList.Add(pOrderLM);
                }
                return pOrdersList;
            }
            return null;
        }

        public dynamic GetFranchiseeOrder(ulong poId, ulong cUserId, OrderStatus os_Id)
        {
            DataSet dataSet = ipurchaseProductRepository.GetFranchiseeOrder(poId, cUserId, os_Id);

            if (dataSet.Tables[0].Rows.Count > 0 && dataSet.Tables[0] != null)
            {
                return BindPOWithProducts(dataSet);

            }
            return null;
        }

        private dynamic BindPOWithProducts(DataSet dataSet)
        {
            DataTable dataTable = dataSet.Tables[0];
            POProductModel poProductModel = new POProductModel();
            poProductModel.Id = Convert.ToUInt64(dataTable.Rows[0]["po_id"]);
            poProductModel.BillNo = Convert.ToUInt64(dataTable.Rows[0]["fc_po_id"]);
            poProductModel.VendorName = dataTable.Rows[0]["vendor_name"].ToString();
            poProductModel.OrderDate = dataTable.Rows[0]["order_date"].ToString();
            poProductModel.TotalBillAmount = Convert.ToUInt64(dataTable.Rows[0]["total_bill_amount"].ToString());

            if (dataSet.Tables[1].Rows.Count > 0)
            {
                List<POProductListModel> pOrderList = new List<POProductListModel>();
                foreach (DataRow row in dataSet.Tables[1].Rows)
                {
                    POProductListModel poProductLM = new POProductListModel();
                    poProductLM.ProdName = row["prodname"].ToString();
                    poProductLM.Quantity = Convert.ToSingle(row["quantity"]);
                    poProductLM.MRP = Convert.ToSingle(row["mrp"]);
                    poProductLM.Price = Convert.ToSingle(row["price"]);
                    poProductLM.TotalAmount = Convert.ToSingle(row["total_amount"]);
                    pOrderList.Add(poProductLM);
                }
                poProductModel.POPProductList = pOrderList;
            }
            return poProductModel;
        }

    }
}