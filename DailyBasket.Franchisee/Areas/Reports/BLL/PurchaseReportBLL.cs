﻿using DailyBasket.Franchisee.Core.Interface.BLL.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Runtime.CompilerServices;
using DailyBasket.Franchisee.Core.Model.Report.Purchase;
using DailyBasket.Franchisee.Repository.Reports;
using DailyBasket.Franchisee.Core.Model.Report.Franchisee;

namespace DailyBasket.Franchisee.Areas.Reports.BLL
{

    public class PurchaseReportBLL : IPurchaseReport
    {
        private Core.Interface.DAL.Reports.IPurchaseReport iPurchaseReportRepository;
        public PurchaseReportBLL()
        {
            iPurchaseReportRepository = new PurchaseReportRepository();
        }

        public BillWisePurchaseModel GetBillWiseReportList(string billno, ulong userId)
        {
            DataTable dataTable = iPurchaseReportRepository.GetBillWiseReportList(billno, userId);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindBillWisePurchaseModel(dataTable);
            }
            return null;
        }

        public DataTable GetBillWiseReportListforExcel(string billno, ulong userId)
        {
            DataTable dataTable = iPurchaseReportRepository.GetBillWiseReportList(billno, userId);
            dataTable.Columns["vendor_name"].SetOrdinal(0);
            return dataTable;
        }
        private BillWisePurchaseModel BindBillWisePurchaseModel(DataTable dataTable)
        {

            BillWisePurchaseModel billWisePurchaseModel = new BillWisePurchaseModel();
            billWisePurchaseModel.Bill_No = dataTable.Rows[0]["bill_no"].ToString();
            billWisePurchaseModel.Bill_Date = dataTable.Rows[0]["bill_date"].ToString();
            billWisePurchaseModel.Total_Bill_Amount = Convert.ToSingle(dataTable.Rows[0]["total_bill_amount"]);
            billWisePurchaseModel.VendorName = dataTable.Rows[0]["vendor_name"].ToString();
            billWisePurchaseModel.FranchiseeName = dataTable.Rows[0]["franchisee_name"].ToString();
            billWisePurchaseModel.Address = dataTable.Rows[0]["address"].ToString();
            billWisePurchaseModel.City = dataTable.Rows[0]["city"].ToString();
            billWisePurchaseModel.State = dataTable.Rows[0]["state"].ToString();
            billWisePurchaseModel.GSTIN = dataTable.Rows[0]["gstin"].ToString();
            billWisePurchaseModel.Mobile = dataTable.Rows[0]["mobile"].ToString();
            return billWisePurchaseModel;
        }


        public dynamic GetDayWisReportList(string fromdate, string todate, ulong userId)
        {
            DataSet dataSet = iPurchaseReportRepository.GetDayWisReportList(fromdate, todate, userId);
            if (dataSet.Tables[0].Rows.Count > 0)
            {
                return BindDayWisePurchaseModel(dataSet);
            }
            return null;

        }

        public DataTable GetDayWisReportListforExcel(string fromdate, string todate, ulong userId)
        {
            
            DataSet dataSet = iPurchaseReportRepository.GetDayWisReportList(fromdate, todate, userId);
            DataTable dataTable = dataSet.Tables[1];

            var bill_Amount = dataTable.AsEnumerable().Sum(x => x.Field<double>(1));

            DataRow dataRow = dataTable.NewRow();
            dataRow[0] = "Grand Total";
            dataRow[1] = bill_Amount;
            dataTable.Rows.InsertAt(dataRow, dataTable.Rows.Count);
            return dataTable;
        }
        private dynamic BindDayWisePurchaseModel(DataSet dataSet)
        {
            DataTable franchiseeUM = dataSet.Tables[0];
            DateWisePurchaseModel dateWPModel = new DateWisePurchaseModel();
            dateWPModel.FranchiseeName = franchiseeUM.Rows[0]["franchisee_name"].ToString();
            dateWPModel.Address = franchiseeUM.Rows[0]["address"].ToString();
            dateWPModel.City = franchiseeUM.Rows[0]["city"].ToString();
            dateWPModel.State = franchiseeUM.Rows[0]["state"].ToString();
            dateWPModel.GSTIN = franchiseeUM.Rows[0]["gstin"].ToString();
            dateWPModel.Mobile = franchiseeUM.Rows[0]["mobile"].ToString();
            
            DataTable dataTable = dataSet.Tables[1];
            List<DateWisePurchaseReportModel> dtmodel = new List<DateWisePurchaseReportModel>();
            foreach (DataRow Row in dataTable.Rows)
            {
                DateWisePurchaseReportModel dtwprModel = new DateWisePurchaseReportModel();
                dtwprModel.Bill_Date = Row["bill_date"].ToString();
                dtwprModel.Total_Bill_Amount = Convert.ToSingle(Row["total_bill_amount"]);
                dtmodel.Add(dtwprModel);
            }

            dateWPModel.dateWPRModel = dtmodel;
            return dateWPModel;
        }

        public SelectList GetPurchaseReportList()
        {
            throw new NotImplementedException();
        }
    }
}