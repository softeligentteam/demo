﻿using DailyBasket.Franchisee.Core.Interface.BLL.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.CompilerServices;
using DailyBasket.Franchisee.Repository.Reports;

using System.Data;
using System.Web.Mvc;
using DailyBasket.Franchisee.Core.Model.Report.Stock;

namespace DailyBasket.Franchisee.Areas.Reports.BLL
{
    public class StockReportBLL : IStockReport
    {
        Core.Interface.DAL.Reports.IStockReport iStockReportRepository;
        public StockReportBLL()
        {
            iStockReportRepository = new StockReportRepository();
        }

        public dynamic CurrentAvailableStock(uint ProductId, ulong userId)
        {
            DataSet dataSet = iStockReportRepository.CurrentAvailableStock(ProductId, userId);
            if (dataSet != null && dataSet.Tables[1].Rows.Count > 0)
            {
                return BindCurrentAvailableStockModel(dataSet);
            }
            return null;
        }

        public DataTable CurrentAvailableStockforExcel(uint ProductId, ulong userId)
        {
            DataSet dataSet = iStockReportRepository.CurrentAvailableStock(ProductId, userId);
            DataTable dataTable = dataSet.Tables[1];
            return dataTable;
        }
        private dynamic BindCurrentAvailableStockModel(DataSet dataSet)
        {
            DataTable dataTable = dataSet.Tables[0];

            CurrentAvailableStockModel currentASModel = new CurrentAvailableStockModel();
            currentASModel.FranchiseeName = dataTable.Rows[0]["franchisee_name"].ToString();
            currentASModel.Address = dataTable.Rows[0]["address"].ToString();
            currentASModel.City = dataTable.Rows[0]["city"].ToString();
            currentASModel.State = dataTable.Rows[0]["state"].ToString();
            currentASModel.GSTIN = dataTable.Rows[0]["gstin"].ToString();
            currentASModel.Mobile = dataTable.Rows[0]["mobile"].ToString();

            List<CurrentAvailableStockReportModel> currentAvailableStockModel = new List<CurrentAvailableStockReportModel>();
            foreach (DataRow Row in dataSet.Tables[1].Rows)
            {
                CurrentAvailableStockReportModel camodel = new CurrentAvailableStockReportModel();
                camodel.prod_name = Row["prod_name"].ToString();
                camodel.purchase_rate = Convert.ToSingle(Row["purchase_rate"]);
                camodel.quantity = Convert.ToSingle(Row["quantity"]);
                camodel.mrp = Convert.ToSingle(Row["mrp"]);
                camodel.sale_rate = Convert.ToSingle(Row["sale_rate"]);
                camodel.batch_no = Row["batch_no"].ToString();
                currentAvailableStockModel.Add(camodel);
            }
            currentASModel.CurrentASRModel = currentAvailableStockModel;
            return currentASModel;
        }

        public dynamic GetClosingStockReportList(string fromdate, uint ProductId, ulong userId)
        {
            DataSet dataSet = iStockReportRepository.GetClosingStockReportList(fromdate, ProductId, userId);
            if (dataSet != null && dataSet.Tables[1].Rows.Count > 0)
            {
                return BindClosingStockReportList(dataSet);
            }
            return null;
        }

        public DataTable GetClosingStockReportListforExcel(string fromdate, uint ProductId, ulong userId)
        {
            DataSet dataSet = iStockReportRepository.GetClosingStockReportList(fromdate, ProductId, userId);
            DataTable dataTable = dataSet.Tables[1];
            return dataTable;
            
        }
        private dynamic BindClosingStockReportList(DataSet dataSet)
        {
            DataTable dataTable = dataSet.Tables[0];

            ClosingStockModel closingSModel = new ClosingStockModel();
            closingSModel.FranchiseeName = dataTable.Rows[0]["franchisee_name"].ToString();
            closingSModel.Address = dataTable.Rows[0]["address"].ToString();
            closingSModel.City = dataTable.Rows[0]["city"].ToString();
            closingSModel.State = dataTable.Rows[0]["state"].ToString();
            closingSModel.GSTIN = dataTable.Rows[0]["gstin"].ToString();
            closingSModel.Mobile = dataTable.Rows[0]["mobile"].ToString();

            List<ClosingStockReportModel> closingStockRModel = new List<ClosingStockReportModel>();
            foreach (DataRow Row in dataSet.Tables[1].Rows)
            {
                ClosingStockReportModel clmodel = new ClosingStockReportModel();
                clmodel.prod_name = Row["prod_name"].ToString();
                clmodel.purchase_rate = Convert.ToSingle(Row["purchase_rate"]);
                clmodel.quantity = Convert.ToSingle(Row["quantity"]);
                clmodel.mrp = Convert.ToSingle(Row["mrp"]);
                clmodel.online_sale_rate = Convert.ToSingle(Row["sale_rate"]);
                clmodel.batch_no = Row["batch_no"].ToString();
                closingStockRModel.Add(clmodel);
            }
            closingSModel.ClosingSRModel = closingStockRModel;
            return closingSModel;

        }

        public dynamic ExpireWiseStock(uint ProductId, ulong userId)
        {
            DataSet dataSet = iStockReportRepository.ExpireWiseStock(ProductId, userId);
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                return BindExpireWiseStockModel(dataSet);
            }
            return null;
        }

        public DataTable ExpireWiseStockforExcel(uint ProductId, ulong userId)
        {
            DataSet dataSet = iStockReportRepository.ExpireWiseStock(ProductId, userId);
            DataTable dataTable = dataSet.Tables[1];
            return dataTable;
        }
        private dynamic BindExpireWiseStockModel(DataSet dataSet)
        {
            DataTable dataTable = dataSet.Tables[0];

            ExpireWiseStockModel expiredWSModel = new ExpireWiseStockModel();
            expiredWSModel.FranchiseeName = dataTable.Rows[0]["franchisee_name"].ToString();
            expiredWSModel.Address = dataTable.Rows[0]["address"].ToString();
            expiredWSModel.City = dataTable.Rows[0]["city"].ToString();
            expiredWSModel.State = dataTable.Rows[0]["state"].ToString();
            expiredWSModel.GSTIN = dataTable.Rows[0]["gstin"].ToString();
            expiredWSModel.Mobile = dataTable.Rows[0]["mobile"].ToString();
            List<ExpireWiseStockReportModel> expireWiseStockModel = new List<ExpireWiseStockReportModel>();
            foreach (DataRow Row in dataSet.Tables[1].Rows)
            {
                ExpireWiseStockReportModel exmodel = new ExpireWiseStockReportModel();
                exmodel.prod_name = Row["prod_name"].ToString();
                exmodel.batch_no = Row["batch_no"].ToString();
                exmodel.expire_date = Row["expiry_date"].ToString();
                exmodel.stock_quantity = Row["quantity"].ToString();
                //exmodel.vendor_name= Row["vendor_name"].ToString();
                expireWiseStockModel.Add(exmodel);
            }
            expiredWSModel.ExpireWSRModel = expireWiseStockModel;
            return expiredWSModel;
        }
    }
}