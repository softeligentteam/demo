﻿using DailyBasket.Franchisee.Core.Interface.BLL.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.CompilerServices;
using System.Web.Mvc;
using DailyBasket.Franchisee.Repository.Reports;

using DailyBasket.Franchisee.Areas.Reports.Models;
using System.Data;
using DailyBasket.Franchisee.Core.Model.Report.Sales;

namespace DailyBasket.Franchisee.Areas.Reports.BLL
{
    public class SalesReportBLL : ISalesReport
    {
        Core.Interface.DAL.Reports.ISalesReport iSalesReportRepository;
        public SalesReportBLL()
        {
            iSalesReportRepository = new SalesReportsRepository();
        }

        public dynamic GetBillWiseSalesList(string fromdate, string todate, ulong userId)
        {
            DataSet dataSet = iSalesReportRepository.GetBillWiseSalesList(fromdate, todate, userId);
            if (dataSet != null && dataSet.Tables[1].Rows.Count > 0)
            {
                return BindBillWiseSalesList(dataSet);
            }
            return null;
        }

        private dynamic BindBillWiseSalesList(DataSet dataSet)
        {
            DataTable dataTable = dataSet.Tables[0];

            BillWiseSalesModel billWiseSalesModel = new BillWiseSalesModel();
            billWiseSalesModel.FranchiseeName = dataTable.Rows[0]["franchisee_name"].ToString();
            billWiseSalesModel.Address = dataTable.Rows[0]["address"].ToString();
            billWiseSalesModel.City = dataTable.Rows[0]["city"].ToString();
            billWiseSalesModel.State = dataTable.Rows[0]["state"].ToString();
            billWiseSalesModel.GSTIN = dataTable.Rows[0]["gstin"].ToString();
            billWiseSalesModel.Mobile = dataTable.Rows[0]["mobile"].ToString();

            List<BillWiseSalesReportModel> billWSalesModel = new List<BillWiseSalesReportModel>();
            DataTable billwiseSM = dataSet.Tables[1];
            foreach (DataRow Row in billwiseSM.Rows)
            {
                BillWiseSalesReportModel billmodel = new BillWiseSalesReportModel();
                // mcmodel.MainCategoryId = Convert.ToUInt64(dataTable.Rows[0]["bill_no"].ToString());
                billmodel.First_Name = Row["first_name"].ToString();
                billmodel.Middle_Name = Row["middle_name"].ToString();
                billmodel.Last_Name = Row["last_name"].ToString();
                billmodel.Order_Date= Row["order_date"].ToString();
                billmodel.Corder_Id = Convert.ToUInt64(Row["corder_id"].ToString());
                billmodel.Total_Order_Amount = Convert.ToSingle(Row["total_order_amount"]);
                billWSalesModel.Add(billmodel);
            }
            billWiseSalesModel.billWSRModel = billWSalesModel;
            return billWiseSalesModel;
        }
        public dynamic GetBillWiseSalesListForExcel(string fromdate, string todate, ulong userId)
        {
            DataSet dataSet = iSalesReportRepository.GetBillWiseSalesList(fromdate, todate, userId);
            DataTable dataTable = new DataTable();
            dataTable = dataSet.Tables[1];
            var total_order_amount = dataTable.AsEnumerable().Sum(x => x.Field<double>(5));
          
            dataTable.Columns["corder_id"].SetOrdinal(3);
            DataRow dataRow = dataTable.NewRow();
            dataRow[4] = "Grand Total";
            dataRow[5] = total_order_amount;
            dataTable.Rows.InsertAt(dataRow, dataTable.Rows.Count);
            return dataTable;
        }
      
          
        public dynamic GetDateWiseCustomerOrderList(string fromdate, string todate, ulong userId)
        {
            DataSet dataSet = iSalesReportRepository.GetDateWiseCustomerOrderList(fromdate, todate, userId);
            if (dataSet != null && dataSet.Tables[1].Rows.Count > 0)
            {
                return BindDateWiseCustomerOrderSalesModel(dataSet);
            }
            return null;
        }

        public DataTable GetDateWiseCustomerOrderListforExcel(string fromdate, string todate, ulong userId)
        {
            DataSet dataSet = iSalesReportRepository.GetDateWiseCustomerOrderList(fromdate, todate, userId);
            DataTable dataTable = dataSet.Tables[1];
            var grandTotal = dataTable.AsEnumerable().Sum(x => x.Field<double>(1));

            DataRow dataRowQuantity = dataTable.NewRow();
            dataRowQuantity[0] = "Grand Total";
            dataRowQuantity[1] = grandTotal;
            dataTable.Rows.InsertAt(dataRowQuantity, dataTable.Rows.Count);

            return dataTable;
        }
        private dynamic BindDateWiseCustomerOrderSalesModel(DataSet dataSet)
        {
            DataTable dataTable = dataSet.Tables[0];
            DateWiseCustomerOrderSalesModel dateWiseCOSModel = new DateWiseCustomerOrderSalesModel();
            dateWiseCOSModel.FranchiseeName = dataTable.Rows[0]["franchisee_name"].ToString();
            dateWiseCOSModel.Address = dataTable.Rows[0]["address"].ToString();
            dateWiseCOSModel.City = dataTable.Rows[0]["city"].ToString();
            dateWiseCOSModel.State = dataTable.Rows[0]["state"].ToString();
            dateWiseCOSModel.GSTIN = dataTable.Rows[0]["gstin"].ToString();
            dateWiseCOSModel.Mobile = dataTable.Rows[0]["mobile"].ToString();

            List<DateWiseCustomerOrderSalesReportModel> dateWiseCustomerOrderSalesModel = new List<DateWiseCustomerOrderSalesReportModel>();
            foreach (DataRow Row in dataSet.Tables[1].Rows)
            {
                DateWiseCustomerOrderSalesReportModel dcomodel = new DateWiseCustomerOrderSalesReportModel();
                // mcmodel.MainCategoryId = Convert.ToUInt64(dataTable.Rows[0]["bill_no"].ToString());
                dcomodel.order_date = Row["order_date"].ToString();
                dcomodel.total_order_amount = Convert.ToSingle(Row["total_order_amount"]);

                dateWiseCustomerOrderSalesModel.Add(dcomodel);
            }
            dateWiseCOSModel.dateWCOSRModel = dateWiseCustomerOrderSalesModel;
            return dateWiseCOSModel;
        }

        
        public dynamic GetProductWiseSalesReportList(string fromdate, string todate, uint ProductId, ulong userId)
        {
            DataSet dataSet = iSalesReportRepository.GetProductWiseSalesReportList(fromdate, todate, ProductId, userId);
            if (dataSet != null && dataSet.Tables[1].Rows.Count > 0)
            {
                return BindProductWiseSalesList(dataSet);
            }
            return null;
        }

        public DataTable GetProductWiseSalesReportListforExcel(string fromdate, string todate, uint ProductId, ulong userId)
        {
            DataSet dataSet = iSalesReportRepository.GetProductWiseSalesReportList(fromdate, todate, ProductId, userId);
            DataTable dataTable = dataSet.Tables[1];
            return dataTable;
        }
        private dynamic BindProductWiseSalesList(DataSet dataSet)
        {
            DataTable dataTable = dataSet.Tables[0];

            ProductWiseSalesModel productWSModel = new ProductWiseSalesModel();
            productWSModel.FranchiseeName = dataTable.Rows[0]["franchisee_name"].ToString();
            productWSModel.Address = dataTable.Rows[0]["address"].ToString();
            productWSModel.City = dataTable.Rows[0]["city"].ToString();
            productWSModel.State = dataTable.Rows[0]["state"].ToString();
            productWSModel.GSTIN = dataTable.Rows[0]["gstin"].ToString();
            productWSModel.Mobile = dataTable.Rows[0]["mobile"].ToString();


            List<ProductWiseSalesReportModel> productWiseSalesModel = new List<ProductWiseSalesReportModel>();
            foreach (DataRow Row in dataSet.Tables[1].Rows)
            {
                ProductWiseSalesReportModel pwmodel = new ProductWiseSalesReportModel();
                // mcmodel.MainCategoryId = Convert.ToUInt64(dataTable.Rows[0]["bill_no"].ToString());
                pwmodel.corder_id= Convert.ToUInt64(Row["corder_id"].ToString());
                pwmodel.prod_name = Row["prod_name"].ToString();
                pwmodel.os_id = Convert.ToUInt32(Row["os_id"].ToString());
                pwmodel.order_date= Row["order_date"].ToString();
                pwmodel.first_name= Row["first_name"].ToString();
                pwmodel.middle_name = Row["middle_name"].ToString();
                pwmodel.last_name = Row["last_name"].ToString();
                pwmodel.mobile= Row["mobile"].ToString();
                pwmodel.quantity = Convert.ToSingle(Row["quantity"]);
                pwmodel.sale_rate = Convert.ToSingle(Row["sale_rate"]);
                pwmodel.gst_amount= Convert.ToSingle(Row["gst"]);
                pwmodel.mrp = Convert.ToSingle(Row["mrp"]);
                pwmodel.batch_no= Row["batch_no"].ToString();
                productWiseSalesModel.Add(pwmodel);
            }
            productWSModel.productWSRModel = productWiseSalesModel;
            return productWSModel;
        }

        public SelectList GetSalesReportList()
        {
            throw new NotImplementedException();
        }
    }
}