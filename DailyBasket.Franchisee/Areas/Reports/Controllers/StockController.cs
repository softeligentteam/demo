﻿using DailyBasket.Franchisee.Areas.ProductMaster.BLL;
using DailyBasket.Franchisee.Areas.Reports.BLL;
using DailyBasket.Franchisee.Areas.Reports.Models;
using DailyBasket.Franchisee.Areas.Reports.Models.ViewModels;
using DailyBasket.Franchisee.Core.Model.Report.Stock;
using DailyBasket.Franchisee.Helper;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.text.html.simpleparser;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using ClosedXML.Excel;

namespace DailyBasket.Franchisee.Areas.Reports.Controllers
{
    public class StockController : Controller
    {
        StockReportBLL StockReportBLL = new StockReportBLL();
        public ActionResult Stock()
        {
            BindstockReportDropdownList();
            return View();
        }
        private void BindstockReportDropdownList()
        {
            ViewBag.PurchaseReportDdlList = EnumDropdownlistHelper.GetEnumStockReportList();
        }
        [HttpPost]
        public ActionResult Stock(StockReportList StockReportList)
        {
            BindstockReportDropdownList();
            var rptname = (StockReportList.Report).ToString();

            switch (rptname)
            {
                case "ClosingStock":

                    return RedirectToAction("ClosingStock");
                case "CurrentAvailableStock":

                    return RedirectToAction("CurrentAvailableStock");
                case "ExpireWiseStock":
                    return RedirectToAction("ExpireWiseStock");

                default:
                    return PartialView("Stock");
            }

        }
        [HttpGet]
        public ActionResult ClosingStock()
        {
            ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
            return PartialView("ClosingStockWise");
        }
        [HttpGet]
        public ActionResult CurrentAvailableStock()
        {
            ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
            return PartialView("CurrentStockAvailableList");
        }

        [HttpGet]
        public ActionResult ExpireWiseStock()
        {
            ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
            return PartialView("ExpireWiseStock");
        }
        // GET: Reports/Stock
        [HttpPost]
        public ActionResult ExpireWiseStockReport(string Submit, uint ProductId = 0)
        {
            switch (Submit)
            {
                case "Export To Excel":
                    DataTable closingStock = null;
                    closingStock = StockReportBLL.ExpireWiseStockforExcel(ProductId, SessionManager.CurrentUserId);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(closingStock, "ExpireWiseStockReport");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=ExpireWiseStockReport.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                    return RedirectToAction("ExpireWiseStock");
                default:
                    ExpireWiseStockModel expireWiseStockModel = null;
                    expireWiseStockModel = StockReportBLL.ExpireWiseStock(ProductId, SessionManager.CurrentUserId);
                    var HTMLContent = RenderRazorViewToString(expireWiseStockModel, "ExpireWiseStockReport");
                    var bytes = GetPDF(HTMLContent);

                    using (MemoryStream input = new MemoryStream(bytes))
                    {

                        var reader = new PdfReader(input);

                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-disposition", "attachment;filename=ExpireWiseStockReport.pdf");
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.BinaryWrite(bytes);
                        Response.End();

                    }
                    return RedirectToAction("ExpireWiseStock");
            }
        }
        [HttpPost]
        public ActionResult ClosingStock(string Submit, string From_Date, uint ProductId = 0)
        {
            switch (Submit)
            {
                case "Export To Excel":
                    DataTable closingStock = null;
                    closingStock = StockReportBLL.GetClosingStockReportListforExcel(From_Date, ProductId, SessionManager.CurrentUserId);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(closingStock, "ClosingStock");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=CloseStock Report.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                    return RedirectToAction("ClosingStock");
                default:
                    ClosingStockModel closingStockModel = null;
                    closingStockModel = StockReportBLL.GetClosingStockReportList(From_Date, ProductId, SessionManager.CurrentUserId);

                    var HTMLContent = RenderRazorViewToString(closingStockModel, "ClosingStock");

                    var bytes = GetPDF(HTMLContent);
                    using (MemoryStream input = new MemoryStream(bytes))
                    {
                        var reader = new PdfReader(input);
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-disposition", "attachment;filename=CloseStock Report.pdf");
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.BinaryWrite(bytes);
                        Response.End();

                    }
                    return RedirectToAction("ClosingStock");
            }
        }

        [HttpPost]
        public ActionResult currentAvailableStockModel(string Submit, uint ProductId = 0)
        {
            switch (Submit)
            {
                case "Export To Excel":
                    DataTable currentAvailableStock = null;
                    currentAvailableStock = StockReportBLL.CurrentAvailableStockforExcel(ProductId, SessionManager.CurrentUserId);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(currentAvailableStock, "CurrentStock");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=CurrentStock Report.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                    return RedirectToAction("currentAvailableStockModel");
                default:
                    CurrentAvailableStockModel currentAvailableStockModel = null;
                    currentAvailableStockModel = StockReportBLL.CurrentAvailableStock(ProductId, SessionManager.CurrentUserId);
                    var HTMLContent = RenderRazorViewToString(currentAvailableStockModel, "CurrentStock");
                    var bytes = GetPDF(HTMLContent);

                    using (MemoryStream input = new MemoryStream(bytes))
                    {

                        var reader = new PdfReader(input);

                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-disposition", "attachment;filename=CurrentStock Report.pdf");
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.BinaryWrite(bytes);
                        Response.End();

                    }
                    return RedirectToAction("currentAvailableStockModel");
            }
        }
        public byte[] GetPDF(string pHTML)
        {
            byte[] bPDF = null;

            var ms = new MemoryStream();
            var txtReader = new StringReader(pHTML);

            // 1: create object of a itextsharp document class  
            var doc = new Document(PageSize.A4, 25, 25, 25, 25);

            // 2: we create a itextsharp pdfwriter that listens to the document and directs a XML-stream to a file  
            var oPdfWriter = PdfWriter.GetInstance(doc, ms);

            // 3: we create a worker parse the document  
            var htmlWorker = new HTMLWorker(doc);

            // 4: we open document and start the worker on the document  
            doc.Open();
            htmlWorker.StartDocument();

            // 5: parse the html into the document  
            htmlWorker.Parse(txtReader);

            // 6: close the document and the worker  
            htmlWorker.EndDocument();
            htmlWorker.Close();
            doc.Close();

            bPDF = ms.ToArray();

            return bPDF;
        }

        public string RenderRazorViewToString(dynamic viewModel, string viewName)
        {

            ViewData["viewModel"] = viewModel;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);

                return sw.GetStringBuilder().ToString();
            }
        }

        // [HttpPost]
        // public ActionResult CurrentAvailableStock(uint ProductId = 0)
        // {
        //return RedirectToAction("CurrentAvailableStock");

        // }
    }
}