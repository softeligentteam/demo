﻿using DailyBasket.Franchisee.Areas.ProductMaster.BLL;
using DailyBasket.Franchisee.Areas.Reports.BLL;
using DailyBasket.Franchisee.Areas.Reports.Models;
using DailyBasket.Franchisee.Areas.Reports.Models.ViewModels;
using DailyBasket.Franchisee.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DailyBasket.Franchisee.Core.Interface.BLL.Reports;
using DailyBasket.Franchisee.Core.Model.Report.Sales;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.text.html.simpleparser;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;

namespace DailyBasket.Franchisee.Areas.Reports.Controllers
{
    public class SaleController : Controller
    {
        SalesReportBLL SalesReportBLL = new SalesReportBLL();
        public ActionResult Sales()
        {
            BindPurchaseReportDropdownList();
            return View();
        }
        [HttpPost]
        public ActionResult Sales(SalesReportList SalesReportList)
        {
            BindPurchaseReportDropdownList();
            var rptname = (SalesReportList.Report).ToString();

            switch (rptname)
            {
                case "BillWise":
                    return RedirectToAction("BillWise");
                case "DateWiseCustomerOrder":
                    return RedirectToAction("DateWiseCustomerOrder");
                case "ProductWiseSales":
                    return RedirectToAction("ProductWiseSales");

                default:
                    return RedirectToAction("Sales");
            }

        }

        [HttpGet]
        public ActionResult BillWise()
        {
            return PartialView("BillWiseSales");
        }

        [HttpGet]
        public ActionResult DateWiseCustomerOrder()
        {
            return PartialView("DateWiseCustomerOrder");
        }
        [HttpGet]
        public ActionResult ProductWiseSales()
        {
            ViewBag.ProductDdlList = new ProductBLL().GetActiveProductListDdl();
            return PartialView("ProductWiseSales");
        }

        public byte[] GetPDF(string pHTML)
        {
            byte[] bPDF = null;

            var ms = new MemoryStream();
            var txtReader = new StringReader(pHTML);

            // 1: create object of a itextsharp document class  
            var doc = new Document(PageSize.A4, 25, 25, 25, 25);

            // 2: we create a itextsharp pdfwriter that listens to the document and directs a XML-stream to a file  
            var oPdfWriter = PdfWriter.GetInstance(doc, ms);

            // 3: we create a worker parse the document  
            var htmlWorker = new HTMLWorker(doc);

            // 4: we open document and start the worker on the document  
            doc.Open();
            htmlWorker.StartDocument();

            // 5: parse the html into the document  
            htmlWorker.Parse(txtReader);

            // 6: close the document and the worker  
            htmlWorker.EndDocument();
            htmlWorker.Close();
            doc.Close();

            bPDF = ms.ToArray();

            return bPDF;
        }

        public string RenderRazorViewToString(dynamic viewModel, string viewName)
        {

            ViewData["viewModel"] = viewModel;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);

                return sw.GetStringBuilder().ToString();
            }
        }

        [HttpPost]
        public ActionResult productWiseSalesModel(string Submit, string From_Date, string To_Date, uint ProductId = 0)
        {
            switch (Submit)
            {
                case "Export To Excel":
                    DataTable productWiseSales = null;
                    productWiseSales = SalesReportBLL.GetProductWiseSalesReportListforExcel(From_Date, To_Date, ProductId, SessionManager.CurrentUserId);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(productWiseSales, "productWiseSales");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=ProductWise Sales Report.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                    return RedirectToAction("ProductWiseSales");
                default:
                    ProductWiseSalesModel productWiseSalesModel = null;
                    productWiseSalesModel = SalesReportBLL.GetProductWiseSalesReportList(From_Date, To_Date, ProductId, SessionManager.CurrentUserId);
                    var HTMLContent = RenderRazorViewToString(productWiseSalesModel, "ProductWiseSalesData");
                    var bytes = GetPDF(HTMLContent);

                    using (MemoryStream input = new MemoryStream(bytes))
                    {

                        var reader = new PdfReader(input);

                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-disposition", "attachment;filename=ProductWise Sales Report.pdf");
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.BinaryWrite(bytes);
                        Response.End();

                    }
                    return RedirectToAction("ProductWiseSales");
            }
        }
        [HttpPost]
        public ActionResult ProductWiseSales(string From_Date, string To_Date, uint ProductId = 0)
        {
            return RedirectToAction("ProductWiseSales");
        }

        [HttpPost]
        public ActionResult BillWise(string From_Date, string To_Date, string Submit)
        {
            switch (Submit)
            {
                case "Export To Excel":
                    DataTable billWiseSales = null;
                    billWiseSales = SalesReportBLL.GetBillWiseSalesListForExcel(From_Date, To_Date, SessionManager.CurrentUserId);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(billWiseSales, "Customers");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=Billwise Sales Report.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                    return RedirectToAction("BillWise");
                default:
                    BillWiseSalesModel billWiseSalesModel = null;
                    billWiseSalesModel = SalesReportBLL.GetBillWiseSalesList(From_Date, To_Date, SessionManager.CurrentUserId);
                    var HTMLContent = RenderRazorViewToString(billWiseSalesModel, "BillWise");
                    var bytes = GetPDF(HTMLContent);
                    using (MemoryStream input = new MemoryStream(bytes))
                    {
                        var reader = new PdfReader(input);
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-disposition", "attachment;filename=Billwise Sales Report.pdf");
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.BinaryWrite(bytes);
                        Response.End();
                    }
                    return RedirectToAction("BillWise");
            }
            //return RedirectToAction("BillWise");
        }

        [HttpPost]
        public ActionResult DateWiseCustomerOrderSalesModel(string From_Date, string To_Date, string Submit)
        {
            switch (Submit)
            {
                case "Export To Excel":
                    DataTable dateWiseCustomerOrderSales = null;
                    dateWiseCustomerOrderSales = SalesReportBLL.GetDateWiseCustomerOrderListforExcel(From_Date, To_Date, SessionManager.CurrentUserId);
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(dateWiseCustomerOrderSales, "dateWiseCustomerOrderSales");
                        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        wb.Style.Font.Bold = true;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=DateWiseCustomerOrder Sales Report.xlsx");

                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                    return View(dateWiseCustomerOrderSales);
                default:
                    DateWiseCustomerOrderSalesModel dateWiseCustomerOrderSalesModel = null;
                    dateWiseCustomerOrderSalesModel = SalesReportBLL.GetDateWiseCustomerOrderList(From_Date, To_Date, SessionManager.CurrentUserId);
                    var HTMLContent = RenderRazorViewToString(dateWiseCustomerOrderSalesModel, "dateWiseCustomerOrderSalesModel");
                    var bytes = GetPDF(HTMLContent);

                    using (MemoryStream input = new MemoryStream(bytes))
                    {

                        var reader = new PdfReader(input);

                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-disposition", "attachment;filename=DateWiseCustomerOrder Sales Report.pdf");
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.BinaryWrite(bytes);
                        Response.End();

                    }
                    return View(dateWiseCustomerOrderSalesModel);
            }


        }
        [HttpPost]
        public ActionResult DateWiseCustomerOrder(string From_Date, string To_Date)
        {

            return RedirectToAction("DateWiseCustomerOrder");
        }

        private void BindPurchaseReportDropdownList()
        {
            ViewBag.PurchaseReportDdlList = EnumDropdownlistHelper.GetEnumSalesReportList();
        }
    }

}