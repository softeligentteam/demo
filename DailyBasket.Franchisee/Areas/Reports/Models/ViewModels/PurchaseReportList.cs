﻿using DailyBasket.Franchisee.Core.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyBasket.Franchisee.Areas.Reports.Models.ViewModels
{
    public class PurchaseReportList
    {
        public PurchaseReportEnum selectedId { get; set; }
        public PurchaseReportEnum Report { get; set; }
        public string From_Date { get; set; }
        public string To_Date { get; set; }
        public string Bill_No { get; set; }
        public int vendor_id { get; set; }
        public int prod_id { get; set; }
        public string VendorName { get; set; }

        public string ProductName { get; set; }
    }
}