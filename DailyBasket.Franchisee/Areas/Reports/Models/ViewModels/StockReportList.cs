﻿using DailyBasket.Franchisee.Core.Enum;

namespace DailyBasket.Franchisee.Areas.Reports.Models.ViewModels
{
    public class StockReportList
    {
        public StockReport Report { get; set; }
        public string From_Date { get; set; }
    }
}