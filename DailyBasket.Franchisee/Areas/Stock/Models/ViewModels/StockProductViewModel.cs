﻿using DailyBasket.Franchisee.Core.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DailyBasket.Franchisee.Areas.Stock.Models.ViewModels
{
    public class StockProductViewModel
    {
        public ulong? Id { get; set; }
        public ulong PurchaseOrderId { get; set; }
        public ulong PopId{ get; set; }
        public ulong SpId { get; set; }
        public ulong ProductId { get; set; }
        public string ProductName { get; set; }
        public string Quantity { get; set; }
        public QuantityType QuantityType { get; set; }
        public string OnlineSaleRate { get; set; }
        public string SaleRateA { get; set; }
        public string SaleRateB { get; set; }
        public string MRP { get; set; }
        public string Remark { get; set; }
        public string RackNo { get; set; }
        public string BatchNo { get; set; }
        public string ExpiryDate { get; set; }
    }
}