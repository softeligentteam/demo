using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyBasket.Franchisee.Areas.Stock.Models.ViewModels
{
    public class ProductSaleRateViewModel 
    {
        public ulong? ProductId { get; set; }
        public float Mrp { get; set; } 
        public float OnlineSaleRate { get; set; }
        public float SaleRateA { get; set; }
        public float SaleRateB { get; set; }
    }
}