﻿using DailyBasket.Franchisee.Core.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyBasket.Franchisee.Areas.Stock.Models.ViewModels
{
    // Expiry Date Wise Convertible Stock Product
    public class ECStockProductViewModel
    {
        public ulong StockProductId { get; set; }
        public ulong ProductId { get; set; }
        public string ProductName { get; set; }
        public float AvailableQuantity { get; set; }
        public QuantityType QuantityType { get; set; }
        public string BatchNo { get; set; }
        public string ExpiryDate { get; set; }
        public string RackNo { get; set; }
    }
}