﻿using System;
using System.Collections.Generic;
using System.Data;
using DailyBasket.Franchisee.Repository.Stock;
using DailyBasket.Franchisee.Areas.Stock.Models.ViewModels;
using DailyBasket.Franchisee.Core.Enum;
using DailyBasket.Franchisee.Core.Model.Stock;
using System.Runtime.CompilerServices;
using DailyBasket.Franchisee.Helper;

namespace DailyBasket.Franchisee.Areas.Stock.BLL
{
    public class StockProductBLL : Franchisee.Core.Interface.BLL.Stock.IStockProduct
    {
        Franchisee.Core.Interface.DAL.Stock.IStockProduct istockProductDAL;
        public StockProductBLL()
        {
            istockProductDAL = new StockProductRepository();
        }

        public dynamic GetBatchWiseProductStock(ulong productId, ulong cUserId)
        {
            DataTable dataTable = istockProductDAL.GetBatchWiseProductStock(productId, cUserId);
            if (dataTable != null || dataTable.Rows.Count > 0)
            {
                List<StockProductViewModel> stockProductVMList = new List<StockProductViewModel>();
                foreach (DataRow row in dataTable.Rows)
                {
                    stockProductVMList.Add(new StockProductViewModel
                    {
                        SpId = Convert.ToUInt32(row["sp_id"]),
                        PopId = Convert.ToUInt32(row["fc_po_id"]),
                        ProductId = Convert.ToUInt32(row["prod_id"]),
                        ProductName = row["prod_name"].ToString(),
                        Quantity = row["quantity"].ToString(),
                        SaleRateA = row["sale_rate"].ToString(),
                        BatchNo = row["batch_no"].ToString()

                    });
                }
                return stockProductVMList;
            }

            return null;
        }

        public dynamic GetExpiryWiseProductsStock(ulong productId, ulong cUserId)
        {
            DataTable dataTable = istockProductDAL.GetExpiryWiseProductsStock(productId, cUserId);
            if (dataTable != null)
            {
                List<StockProductViewModel> stockProductVMList = new List<StockProductViewModel>();
                foreach (DataRow row in dataTable.Rows)
                {

                    stockProductVMList.Add(new StockProductViewModel
                    {
                        //Id = Convert.ToUInt32(row["sp_id"]),
                        ProductId = Convert.ToUInt32(row["prod_id"]),
                        ProductName = row["prod_name"].ToString(),
                        Quantity = row["quantity"].ToString(),
                        //QuantityType = (QuantityType)Enum.Parse(typeof(QuantityType), row["quantity_type"].ToString()),
                        //BatchNo = row["batch_no"].ToString(),
                        ExpiryDate = row["expiry_date"].ToString()
                        
                    });
                }
                return stockProductVMList;
            }

            return null;
        }

        public dynamic GetProductsStock(ulong cUserId)
        {
            DataTable dataTable = istockProductDAL.GetProductsStock(cUserId);

            if (dataTable != null)
            {
                List<StockProductViewModel> stockProductVMList = new List<StockProductViewModel>();
                foreach (DataRow row in dataTable.Rows)
                {

                    stockProductVMList.Add(new StockProductViewModel
                    {
                        ProductId = Convert.ToUInt64(row["prod_id"]),
                        ProductName = row["prod_name"].ToString(),
                        Quantity = row["available_stock"].ToString(),
                        QuantityType = (QuantityType)Enum.Parse(typeof(QuantityType), row["quantity_type"].ToString())
                    });
                }
                return stockProductVMList;
            }
            return null;
        }

        public dynamic GetBatchWiseProductSaleRateToUpdate(ulong popId, ulong spId, ulong cUserId)
        {
            DataTable dataTable = istockProductDAL.GetBatchWiseProductSaleRateToUpdate(popId, spId, cUserId);
            if (dataTable != null || dataTable.Rows.Count > 0)
            {
                StockProductSaleRateModel stockProductModel = new StockProductSaleRateModel();
                stockProductModel.SpId = Convert.ToUInt64(dataTable.Rows[0]["sp_id"].ToString());
                stockProductModel.PopId = Convert.ToUInt64(dataTable.Rows[0]["fc_po_id"].ToString());
                stockProductModel.ProductId = Convert.ToUInt64(dataTable.Rows[0]["prod_id"].ToString());
                stockProductModel.ProductName = dataTable.Rows[0]["prod_name"].ToString();
                stockProductModel.AvailableStock = Convert.ToSingle(dataTable.Rows[0]["available_stock"]);
                stockProductModel.PurchaseRate = Convert.ToSingle(dataTable.Rows[0]["price"]);
                stockProductModel.MRP = Convert.ToSingle(dataTable.Rows[0]["mrp"]);
                stockProductModel.OnlineSaleRate = Convert.ToSingle(dataTable.Rows[0]["sale_rate"]);
                stockProductModel.BatchNo = dataTable.Rows[0]["batch_no"].ToString();
                stockProductModel.ExpiryDate = dataTable.Rows[0]["expiry_date"].ToString();
                stockProductModel.PackagingDate = dataTable.Rows[0]["packaging_date"].ToString();
                return stockProductModel;
            }
            else
            {
                return null;
            }
        }

        public int UpdateBatchWiseProductSaleRate(StockProductSaleRateModel stockProductModel, ulong userId)
        {
            return istockProductDAL.UpdateBatchWiseProductSaleRate(stockProductModel, userId);
        }

       
    }
}