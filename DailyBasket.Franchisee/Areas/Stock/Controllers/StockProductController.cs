﻿using DailyBasket.Franchisee.Areas.ProductMaster.BLL;
using DailyBasket.Franchisee.Areas.Stock.BLL;
using DailyBasket.Franchisee.Areas.Stock.Models.ViewModels;
using DailyBasket.Franchisee.Core.Interface.BLL.Stock;
using DailyBasket.Franchisee.Core.Model.Stock;
using DailyBasket.Franchisee.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyBasket.Franchisee.Areas.Stock.Controllers
{
    [Authorize]
    public class StockProductController : Controller
    {
        // GET: Sale/StockProduct
        IStockProduct istockProductBLL = null;
        public StockProductController()
        {
            istockProductBLL = new StockProductBLL();
        }

        public ActionResult Index()
        {
            return RedirectToAction("AvailableStocks");
        }

        public ActionResult AvailableStocks()
        {
            List<StockProductViewModel> stockProductVMList = istockProductBLL.GetProductsStock(SessionManager.CurrentUserId);
            return View("ProductsStock", stockProductVMList);
        }


        public ActionResult ExpiryWiseProductStock(ulong ProductId = 0)
        {
            if (ProductId != 0)
            {
                List<StockProductViewModel> stockProductVMList = istockProductBLL.GetExpiryWiseProductsStock(ProductId, SessionManager.CurrentUserId);
                return View("ExpiryWiseProductStock", stockProductVMList);
            }
            return RedirectToAction("Index");
        }

        public ActionResult BatchWiseProductStock(ulong ProductId = 0)
        {
            List<StockProductViewModel> stockProductVMList = null;
            if (ProductId != 0)
            {
                stockProductVMList = istockProductBLL.GetBatchWiseProductStock(ProductId, SessionManager.CurrentUserId);
            }
            ViewBag.ProductDdlList = new ProductBLL().GetActiveProdListDdl();
            return View(stockProductVMList);
        }

        [HttpGet]
        public ActionResult UpdateProductSaleRate(ulong fcpoId = 0, ulong spId = 0)
        {
            if (spId != 0)
            {
                StockProductSaleRateModel stockProductModel = istockProductBLL.GetBatchWiseProductSaleRateToUpdate(fcpoId, spId, SessionManager.CurrentUserId);
                return View("UpdateBatchWiseProductSaleRate", stockProductModel);
            }
            return RedirectToAction("BatchWiseProductStock");
        }
        
        [HttpPost]
        public ActionResult UpdateProductSaleRate(StockProductSaleRateModel stockProductModel)
        {
            if (ModelState.IsValid)
            {
                if (istockProductBLL.UpdateBatchWiseProductSaleRate(stockProductModel, SessionManager.CurrentUserId) != 0)
                    return RedirectToAction("BatchWiseProductStock", new { ProductId = stockProductModel.ProductId });
                TempData["Message"] = "Oop's error occured! Try again";
            }
            StockProductSaleRateModel stockProductModelBackup = istockProductBLL.GetBatchWiseProductSaleRateToUpdate(stockProductModel.PopId, stockProductModel.SpId, SessionManager.CurrentUserId);
            if (stockProductModelBackup != null)
            {
                stockProductModel.ProductName = stockProductModelBackup.ProductName;
                stockProductModel.PurchaseRate = stockProductModelBackup.PurchaseRate;
                stockProductModel.AvailableStock = stockProductModelBackup.AvailableStock;
                stockProductModel.BatchNo = stockProductModelBackup.BatchNo;
                stockProductModel.ExpiryDate = stockProductModelBackup.ExpiryDate;
                stockProductModel.PackagingDate = stockProductModelBackup.PackagingDate;
                return View("UpdateBatchWiseProductSaleRate", stockProductModel);
            }
            return RedirectToAction("BatchWiseProductStock", new { ProductId = stockProductModel.ProductId });
        }

    }
}