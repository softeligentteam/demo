﻿using DailyBasket.Franchisee.Core.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyBasket.Franchisee.Areas.Sale.Models.ViewModels
{
    public class CustomerOrdersViewModels
    {
        public ulong Id { get; set; }

        public string FranchiseeName { get; set; }
        public string Gstin { get; set; }
        public string FAddress { get; set; }
        public string FCity { get; set; }
        public string FState { get; set; }
        public string FMobile { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Locality { get; set; }
        public string City { get; set; }
        public double OrderAmount { get; set; }
        public string OrderDate { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string DeliveryBoyName { get; set; }
        public string GstNo { get; set; }
        public string TransactionId { get; set; }
        public string PaymentMode { get; set; }

        //Cash On Delivery Payment Mode - Cash/Card
        public string CodPaymentMode { get; set; }
        public string OrderPaymentStatus { get; set; }

        /*********** Invoice Fields Start ***********/
        public double SavingAmount { get; set; } 
        public float GstAmount { get; set; }
        public uint QuantityCount { get; set; }
        public float DeliveryCharges { get; set; }
        public float ExtraCharges { get; set; }
        public double ProductTotalAmount { get; set; }
        public double ProductTotalAmountofReturnGoods { get; set; }
        /*********** Invoice Fields End ************/
        public uint OrderStatusId { get; set; }
        public OrderStatus Status { get; set; }
        public List<CustomerOrderProductViewModel> customersOrders { get; set; }
        public List<CustomerOrderProductViewModel> customersOrdersWithReturnGoods { get; set; }
        public bool IsReturnProduct { get; set; }
    }
}