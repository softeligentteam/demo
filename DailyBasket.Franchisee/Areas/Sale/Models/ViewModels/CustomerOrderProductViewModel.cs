﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyBasket.Franchisee.Areas.Sale.Models.ViewModels
{
    public class CustomerOrderProductViewModel
    {
        public ulong Id { get; set; }
        public string ProductName { get; set; }
        public float Quantity { get; set; }
        public double TotalAmount { get; set; }
        public string Expiry_Date { get; set; }
        public float Mrp { get; set; }
        public float Sale_Rate { get; set; }
        public string Barcode { get; set; }
        public ulong IsReturnProduct { get; set; }
        public ulong SpId { get; set; }
        public ulong Cop_Id { get; set; }
    }
}