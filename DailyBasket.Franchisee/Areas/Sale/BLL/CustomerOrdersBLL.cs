﻿using DailyBasket.Franchisee.Core.Interface.BLL.Sale;
using System;
using System.Collections.Generic;
using System.Data;
using DailyBasket.Franchisee.Repository.Sale;
using DailyBasket.Franchisee.Areas.Sale.Models.ViewModels;
using System.Runtime.CompilerServices;
using DailyBasket.Franchisee.Core.Enum;
using DailyBasket.Franchisee.Helper;

namespace DailyBasket.Franchisee.Areas.Sale.BLL
{
    public class CustomerOrdersBLL : ICustomerOrders
    {
        private Core.Interface.DAL.Sale.ICustomerOrders iCustomerOrdersRepository;
        public CustomerOrdersBLL()
        {
            iCustomerOrdersRepository = new CustomerOrdersRepository();
        }

        public dynamic GetCustomerOrder(ulong coid, OrderStatus orderStatus, ulong cuserId)
        {
            DataSet dataSet = iCustomerOrdersRepository.GetCustomerOrder(coid, orderStatus, cuserId);
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                return BindCustomerOrderProduct(dataSet);
            }
            return null;
        }

        private CustomerOrdersViewModels BindCustomerOrderProduct(DataSet dataSet)
        {
            DataTable coDataTable = dataSet.Tables[0];
            CustomerOrdersViewModels customerOrdersVM = new CustomerOrdersViewModels();
            //Customer Details
            customerOrdersVM.Id = Convert.ToUInt64(coDataTable.Rows[0]["corder_id"].ToString());
            customerOrdersVM.FirstName = coDataTable.Rows[0]["first_name"].ToString();
            customerOrdersVM.MiddleName = coDataTable.Rows[0]["middle_name"].ToString();
            customerOrdersVM.LastName = coDataTable.Rows[0]["last_name"].ToString();
            customerOrdersVM.Locality = coDataTable.Rows[0]["locality"].ToString();
            customerOrdersVM.City = coDataTable.Rows[0]["city"].ToString();
            customerOrdersVM.Address = coDataTable.Rows[0]["address"].ToString();
            customerOrdersVM.Mobile = coDataTable.Rows[0]["mobile"].ToString();
            customerOrdersVM.Email = coDataTable.Rows[0]["email"].ToString();
            customerOrdersVM.GstNo = coDataTable.Rows[0]["gstin"].ToString();

            //Customer Order Details
            customerOrdersVM.OrderAmount = Convert.ToDouble(coDataTable.Rows[0]["total_order_amount"]);
            customerOrdersVM.OrderDate = coDataTable.Rows[0]["order_date"].ToString();
            customerOrdersVM.OrderStatusId = Convert.ToUInt32(coDataTable.Rows[0]["os_id"]);

            //Customer Payment Details
            customerOrdersVM.TransactionId = coDataTable.Rows[0]["transactionid"].ToString();
            customerOrdersVM.PaymentMode = coDataTable.Rows[0]["paymentmode"].ToString();
            customerOrdersVM.OrderPaymentStatus = coDataTable.Rows[0]["opaymentstatus"].ToString();
            customerOrdersVM.CodPaymentMode = coDataTable.Rows[0]["cod_payment_mode"].ToString();

            if (dataSet.Tables[1].Rows.Count > 0)
            {
                List<CustomerOrderProductViewModel> customerOrderVMList = new List<CustomerOrderProductViewModel>();
                foreach (DataRow Row in dataSet.Tables[1].Rows)
                {
                    CustomerOrderProductViewModel customerOrderProductVM = new CustomerOrderProductViewModel();
                    customerOrderProductVM.Id = Convert.ToUInt64(Row["prod_id"]);
                    customerOrderProductVM.SpId = Convert.ToUInt64(Row["sp_id"]);

                    customerOrderProductVM.ProductName = Row["prod_name"].ToString();
                    customerOrderProductVM.Quantity = Convert.ToSingle(Row["quantity"]);
                    customerOrderProductVM.TotalAmount = Convert.ToDouble(Row["total_amount"]);
                    //customerOrderProductVM.Expiry_Date = Row["expiry_date"].ToString();
                    customerOrderProductVM.Mrp = Convert.ToSingle(Row["mrp"].ToString());
                    customerOrderProductVM.Sale_Rate = Convert.ToSingle(Row["sale_rate"].ToString());
                    //customerOrderProductVM.Barcode = Row["barcode"].ToString();
                    customerOrderVMList.Add(customerOrderProductVM);
                }

                customerOrdersVM.customersOrders = customerOrderVMList;
            }
            else { return customerOrdersVM = null; }
            return customerOrdersVM;
        }

        public dynamic GetCustomerOrderWithReturnGoods(ulong coId, OrderStatus orderStatus)
        {
            DataSet dataSet = iCustomerOrdersRepository.GetCustomerOrderWithReturnGoods(coId, orderStatus);
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                return BindCustomerOrderProductWithReturnGoods(dataSet);
            }
            return null;
        }
        private CustomerOrdersViewModels BindCustomerOrderProductWithReturnGoods(DataSet dataSet)
        {
            DataTable coDataTable = dataSet.Tables[0];
            CustomerOrdersViewModels customerOrdersVM = new CustomerOrdersViewModels();
            //Customer Details
            customerOrdersVM.Id = Convert.ToUInt64(coDataTable.Rows[0]["corder_id"].ToString());
            customerOrdersVM.FirstName = coDataTable.Rows[0]["first_name"].ToString();
            customerOrdersVM.MiddleName = coDataTable.Rows[0]["middle_name"].ToString();
            customerOrdersVM.LastName = coDataTable.Rows[0]["last_name"].ToString();
            customerOrdersVM.Locality = coDataTable.Rows[0]["locality"].ToString();
            customerOrdersVM.City = coDataTable.Rows[0]["city"].ToString();
            customerOrdersVM.Address = coDataTable.Rows[0]["address"].ToString();
            customerOrdersVM.Mobile = coDataTable.Rows[0]["mobile"].ToString();
            customerOrdersVM.Email = coDataTable.Rows[0]["email"].ToString();
            customerOrdersVM.DeliveryBoyName = coDataTable.Rows[0]["delb_name"].ToString();
            customerOrdersVM.GstNo = coDataTable.Rows[0]["gstin"].ToString();

            //Customer Order Details
            customerOrdersVM.OrderAmount = Convert.ToDouble(coDataTable.Rows[0]["total_order_amount"]);
            customerOrdersVM.OrderDate = coDataTable.Rows[0]["order_date"].ToString();
            customerOrdersVM.OrderStatusId = Convert.ToUInt32(coDataTable.Rows[0]["os_id"]);

            //Customer Payment Details
            customerOrdersVM.TransactionId = coDataTable.Rows[0]["transactionid"].ToString();
            customerOrdersVM.PaymentMode = coDataTable.Rows[0]["paymentmode"].ToString();
            customerOrdersVM.OrderPaymentStatus = coDataTable.Rows[0]["opaymentstatus"].ToString();
            customerOrdersVM.CodPaymentMode = coDataTable.Rows[0]["cod_payment_mode"].ToString();

            if (dataSet.Tables[1].Rows.Count > 0)
            {
                List<CustomerOrderProductViewModel> customerOrderVMList = new List<CustomerOrderProductViewModel>();
                foreach (DataRow Row in dataSet.Tables[1].Rows)
                {
                    CustomerOrderProductViewModel customerOrderProductVM = new CustomerOrderProductViewModel();
                    customerOrderProductVM.Id = Convert.ToUInt64(Row["prod_id"]);
                    customerOrderProductVM.SpId = Convert.ToUInt64(Row["sp_id"]);
                    customerOrderProductVM.Cop_Id = Convert.ToUInt64(Row["cop_id"]);
                    customerOrderProductVM.ProductName = Row["prod_name"].ToString();
                    customerOrderProductVM.Quantity = Convert.ToSingle(Row["quantity"]);
                    customerOrderProductVM.TotalAmount = Convert.ToDouble(Row["total_amount"]);
                    //customerOrderProductVM.Expiry_Date = Row["expiry_date"].ToString();
                    customerOrderProductVM.Mrp = Convert.ToSingle(Row["mrp"].ToString());
                    customerOrderProductVM.Sale_Rate = Convert.ToSingle(Row["sale_rate"].ToString());
                    //customerOrderProductVM.Barcode = Row["barcode"].ToString();
                    customerOrderVMList.Add(customerOrderProductVM);
                }
                customerOrdersVM.customersOrders = customerOrderVMList;
            }
            else { return customerOrdersVM = null; }
            if (dataSet.Tables[2].Rows.Count > 0)
            {
                List<CustomerOrderProductViewModel> customerOrderVMListWithReturnGoods = new List<CustomerOrderProductViewModel>();
                foreach (DataRow Row in dataSet.Tables[2].Rows)
                {
                    CustomerOrderProductViewModel customerOrderProductVMWithReturnGoods = new CustomerOrderProductViewModel();
                    customerOrderProductVMWithReturnGoods.ProductName = Row["prod_name"].ToString();
                    customerOrderProductVMWithReturnGoods.Quantity = Convert.ToSingle(Row["quantity"]);
                    customerOrderProductVMWithReturnGoods.TotalAmount = Convert.ToDouble(Row["total_amount"]);
                    customerOrderProductVMWithReturnGoods.Sale_Rate = Convert.ToSingle(Row["sale_rate"].ToString());
                    customerOrderProductVMWithReturnGoods.Mrp = Convert.ToSingle(Row["mrp"].ToString());
                    customerOrderVMListWithReturnGoods.Add(customerOrderProductVMWithReturnGoods);
                }
                customerOrdersVM.customersOrdersWithReturnGoods = customerOrderVMListWithReturnGoods;
            }
            return customerOrdersVM;
        }
        public dynamic GetCustomerOrders(OrderStatus orderStatus, ulong cUserId)
        {
            DataTable dataTable = new CustomerOrdersRepository().GetCustomerOrders(orderStatus, cUserId);

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return BindCustomerOrders(dataTable);
            }
            return null;
        }

        private List<CustomerOrdersViewModels> BindCustomerOrders(DataTable dataTable)
        {
            List<CustomerOrdersViewModels> customerOrdersVMList = new List<CustomerOrdersViewModels>();
            foreach (DataRow Row in dataTable.Rows)
            {
                CustomerOrdersViewModels customerOrdersVM = new CustomerOrdersViewModels();
                customerOrdersVM.Id = Convert.ToUInt64(Row["corder_id"].ToString());
                customerOrdersVM.FirstName = Row["first_name"].ToString();
                customerOrdersVM.MiddleName = Row["middle_name"].ToString();
                customerOrdersVM.LastName = Row["last_name"].ToString();
                customerOrdersVM.Locality = Row["locality"].ToString();
                customerOrdersVM.OrderAmount = Convert.ToDouble(Row["total_order_amount"]);
                customerOrdersVM.OrderDate = Row["order_date"].ToString();
                customerOrdersVMList.Add(customerOrdersVM);
            }
            return customerOrdersVMList;
        }

        public dynamic GetActiveOrderStatusDdlList(uint orderStatusId)
        {
            return DropdownlistHelper.GetIntStringDropDownList(iCustomerOrdersRepository.GetActiveOrderStatusDdlList(orderStatusId));
        }

        public int ChangeOrderStatus(ulong customerOrderId, uint orderStatusId, ulong currentUserId)
        {
            return iCustomerOrdersRepository.ChangeOrderStatus(customerOrderId, orderStatusId, currentUserId);
        }

        public dynamic GetCustomerOrderToPrint(ulong customerOrderId, ulong orderStatus, ulong cUserId)
        {
            DataSet dataSet = iCustomerOrdersRepository.GetCustomerOrderToPrint(customerOrderId, orderStatus, cUserId);
            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                return BindInvoiceCustomerOrderProducts(dataSet);
            }
            return null;
        }

        private CustomerOrdersViewModels BindInvoiceCustomerOrderProducts(DataSet dataSet)
        {
            DataTable coDataTable = dataSet.Tables[0];
            DataTable fcoDataTable = dataSet.Tables[2];
            CustomerOrdersViewModels customerOrdersVM = new CustomerOrdersViewModels();
            //Customer Details
            customerOrdersVM.FranchiseeName = fcoDataTable.Rows[0]["franchisee_name"].ToString();
            customerOrdersVM.FAddress = fcoDataTable.Rows[0]["address"].ToString();
            customerOrdersVM.FCity= fcoDataTable.Rows[0]["city"].ToString();
            customerOrdersVM.FState= fcoDataTable.Rows[0]["state"].ToString();
            customerOrdersVM.FMobile = fcoDataTable.Rows[0]["mobile"].ToString();
            customerOrdersVM.Gstin = fcoDataTable.Rows[0]["gstin"].ToString();

            customerOrdersVM.Id = Convert.ToUInt64(coDataTable.Rows[0]["corder_id"].ToString());
            customerOrdersVM.FirstName = coDataTable.Rows[0]["first_name"].ToString();
            customerOrdersVM.MiddleName = coDataTable.Rows[0]["middle_name"].ToString();
            customerOrdersVM.LastName = coDataTable.Rows[0]["last_name"].ToString();
            customerOrdersVM.Locality = coDataTable.Rows[0]["locality"].ToString();
            customerOrdersVM.City = coDataTable.Rows[0]["city"].ToString();
            customerOrdersVM.Address = coDataTable.Rows[0]["address"].ToString();
            customerOrdersVM.Mobile = coDataTable.Rows[0]["mobile"].ToString();
            customerOrdersVM.Email = coDataTable.Rows[0]["email"].ToString();
            customerOrdersVM.GstNo = coDataTable.Rows[0]["gstin"].ToString();

            //Customer Order Details
            customerOrdersVM.DeliveryCharges = Convert.ToSingle(coDataTable.Rows[0]["delivery_charges"]);
            customerOrdersVM.ExtraCharges = Convert.ToSingle(coDataTable.Rows[0]["extra_charges"]);
            customerOrdersVM.GstAmount = Convert.ToSingle(coDataTable.Rows[0]["total_sgst_amount"]) + Convert.ToSingle(coDataTable.Rows[0]["total_cgst_amount"]);
            customerOrdersVM.OrderAmount = Convert.ToDouble(coDataTable.Rows[0]["total_order_amount"]);
            customerOrdersVM.OrderDate = coDataTable.Rows[0]["order_date"].ToString();
            customerOrdersVM.OrderStatusId = Convert.ToUInt32(coDataTable.Rows[0]["os_id"]);

            //Customer Payment Details
            customerOrdersVM.TransactionId = coDataTable.Rows[0]["transactionid"].ToString();
            customerOrdersVM.PaymentMode = coDataTable.Rows[0]["paymentmode"].ToString();
            customerOrdersVM.OrderPaymentStatus = coDataTable.Rows[0]["opaymentstatus"].ToString();

            if (dataSet.Tables[1].Rows.Count > 0)
            {
                List<CustomerOrderProductViewModel> customerOrderVMList = new List<CustomerOrderProductViewModel>();
                foreach (DataRow Row in dataSet.Tables[1].Rows)
                {
                    CustomerOrderProductViewModel customerOrderProductVM = new CustomerOrderProductViewModel();
                    customerOrderProductVM.ProductName = Row["prod_name"].ToString();
                    customerOrderProductVM.Quantity = Convert.ToSingle(Row["quantity"]);
                    customerOrderProductVM.TotalAmount = Convert.ToDouble(Row["total_amount"]);
                    customerOrderProductVM.Sale_Rate = Convert.ToSingle(Row["sale_rate"].ToString());
                    customerOrderProductVM.Mrp = Convert.ToSingle(Row["mrp"].ToString());
                    customerOrderVMList.Add(customerOrderProductVM);

                    customerOrdersVM.QuantityCount = customerOrdersVM.QuantityCount + (uint)customerOrderProductVM.Quantity;
                    customerOrdersVM.ProductTotalAmount = customerOrdersVM.ProductTotalAmount + customerOrderProductVM.TotalAmount;
                    customerOrdersVM.SavingAmount = customerOrdersVM.SavingAmount + CalculateSavingAmount(customerOrderProductVM.Mrp, customerOrderProductVM.Sale_Rate, customerOrderProductVM.Quantity);
                }
                customerOrdersVM.customersOrders = customerOrderVMList;
            }
            else
            {
                return customerOrdersVM = null;
            }
            

            return customerOrdersVM;
        }

        private float CalculateSavingAmount(float mrp, float saleRate, float quantity)
        {
            float difference = mrp - saleRate;
            if (difference > 0)
            {
                return (difference * quantity);
            }
            return 0;
        }

        public int UpdateCustomerLoyaltyPoints(ulong customerOrderId, uint orderStatusId)
        {
            DataTable dataTable = iCustomerOrdersRepository.GetCustomerOrderWithLoyaltyCounterDetails(customerOrderId, orderStatusId);
            float loyaltyPoints = LoyaltyPointHelper.GetCustomerLoyaltyPoints(dataTable);
            if (loyaltyPoints != 0)
            {
                return iCustomerOrdersRepository.UpdateCustomerLoyaltyPoints(dataTable.Rows[0]["referred_referral_code"].ToString(), Convert.ToSingle(String.Format("{0:0.00}", loyaltyPoints)));
            }
            return 0;
        }

        public int UpdateCustomerOrderCodPaymentMode(ulong customerOrderId, string codPaymentMode)
        {
            return iCustomerOrdersRepository.UpdateCustomerOrderCodPaymentMode(customerOrderId, codPaymentMode);
        }

        public int RevertProductsStock(ulong customerOrderId, uint orderStatusId, ulong userId)
        {
            return iCustomerOrdersRepository.RevertProductsStock(customerOrderId, orderStatusId, userId);
        }
        public dynamic ChangeCurrentOrderStatusofReturnProducts(string[] IsReturnProduct)
        {
            return iCustomerOrdersRepository.ChangeCurrentOrderStatusofReturnProducts(IsReturnProduct);
        }

    }
}