﻿using DailyBasket.Franchisee.Areas.Sale.BLL;
using DailyBasket.Franchisee.Areas.Sale.Models.ViewModels;
using DailyBasket.Franchisee.Core.Enum;
using DailyBasket.Franchisee.Core.Interface.BLL.Sale;
using DailyBasket.Franchisee.Helper;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;

namespace DailyBasket.Franchisee.Areas.Sale.Controllers
{
    [Authorize]
    public class CustomerOrdersController : Controller
    {
        private ICustomerOrders iCustomerOrdersBLL = new CustomerOrdersBLL();
        // GET: CustomerOrders/CustomerOrders
        public ActionResult InProcessOrders()
        {
            ViewBag.MethodName = "InProcessOrder";
            ViewBag.PageTitle = "In Process Customer Orders";
            return View("customerOrders", GetCustomerOrders(OrderStatus.InProcess));
        }

        public ActionResult ReadytoDispatchOrders()
        {
            ViewBag.MethodName = "ReadytoDispatchOrder";
            ViewBag.PageTitle = "Ready to Dispatch Customer Orders";
            return View("customerOrders", GetCustomerOrders(OrderStatus.ReadyToDispatch));
        }

        public ActionResult DispatchedOrders()
        {
            ViewBag.MethodName = "DispatchedOrder";
            ViewBag.PageTitle = "Dispatch Customer Orders";
            return View("customerOrders", GetCustomerOrders(OrderStatus.Dispatched));
        }

        public ActionResult DeliveredOrders()
        {
            ViewBag.MethodName = "DeliveredOrder";
            ViewBag.PageTitle = "Delivered Customer Orders";
            return View("customerOrders", GetCustomerOrders(OrderStatus.Delivered));
        }

        public ActionResult CancelledOrders()
        {
            ViewBag.MethodName = "CancelledOrder";
            ViewBag.PageTitle = "Cancelled Customer Orders";
            return View("customerOrders", GetCustomerOrders(OrderStatus.Cancelled));
        }

        public ActionResult InProcessOrder(ulong coId = 0)
        {
            ViewBag.PageTitle = "In Process Customer Order";
            ViewBag.Url = "ChangeCurrentOrderStatus";
            CustomerOrdersViewModels customerOrderModelView = GetCustomerOrder(OrderStatus.InProcess, coId);
            if (customerOrderModelView == null)
                return RedirectToAction("InProcessOrders");
            customerOrderModelView.Status = OrderStatus.InProcess;
            BindOrderStatusDdlList(OrderStatus.InProcess);
            return View("customerOrder", customerOrderModelView);
        }

        public ActionResult ReadytoDispatchOrder(ulong coId = 0)
        {
            ViewBag.PageTitle = "Ready to Dispatch Customer Order";
            ViewBag.Url = "ChangeCurrentOrderStatus";
            CustomerOrdersViewModels customerOrderModelView = GetCustomerOrder(OrderStatus.ReadyToDispatch, coId);
            if (customerOrderModelView == null)
                return RedirectToAction("ReadytoDispatchOrders");
            customerOrderModelView.Status = OrderStatus.ReadyToDispatch;
            BindOrderStatusDdlList(OrderStatus.ReadyToDispatch);
            return View("customerOrder", customerOrderModelView);
        }

        public ActionResult DispatchedOrder(ulong coId = 0)
        {
            ViewBag.PageTitle = "Dispatch Customer Order";
            ViewBag.Url = "ChangeCurrentOrderStatus";
            CustomerOrdersViewModels customerOrderModelView = GetCustomerOrder(OrderStatus.Dispatched, coId);
            if (customerOrderModelView == null)
                return RedirectToAction("DispatchedOrders");
            customerOrderModelView.Status = OrderStatus.Dispatched;
            ViewBag.CodPaymentModeList = EnumDropdownlistHelper.GetEnumCashOnDeliveryPMList();
            BindOrderStatusDdlList(OrderStatus.Dispatched);
            return View("customerOrder", customerOrderModelView);
        }

        public ActionResult DeliveredOrder(ulong coId = 0)
        {
            ViewBag.PageTitle = "Delivered Customer Order";
            ViewBag.Url = "UpdateReturnGoods";
            CustomerOrdersViewModels customerOrderModelView = GetCustomerOrder(OrderStatus.Delivered, coId);
            if (customerOrderModelView == null)
                return RedirectToAction("DeliveredOrders");
            customerOrderModelView.Status = OrderStatus.Delivered;
            BindOrderStatusDdlList(OrderStatus.Delivered);
            return View("customerOrder", customerOrderModelView);
        }

        public ActionResult CancelledOrder(ulong coId = 0)
        {
            ViewBag.PageTitle = "Cancelled Customer Order";
            ViewBag.Url = null;
            CustomerOrdersViewModels customerOrderModelView = GetCustomerOrder(OrderStatus.Cancelled, coId);
            if (customerOrderModelView == null)
                return RedirectToAction("CancelledOrders");
            customerOrderModelView.Status = OrderStatus.Cancelled;
            BindOrderStatusDdlList(OrderStatus.Cancelled);
            return View("customerOrder", customerOrderModelView);
        }

        private List<CustomerOrdersViewModels> GetCustomerOrders(OrderStatus orderStatus)
        {
            return iCustomerOrdersBLL.GetCustomerOrders(orderStatus, SessionManager.CurrentUserId);
        }

        private CustomerOrdersViewModels GetCustomerOrder(OrderStatus orderStatus, ulong coId)
        {
            return iCustomerOrdersBLL.GetCustomerOrder(coId, orderStatus, SessionManager.CurrentUserId);
        }

        private CustomerOrdersViewModels GetCustomerOrderWithReturnGoods(OrderStatus orderStatus, ulong coId)
        {
            return iCustomerOrdersBLL.GetCustomerOrderWithReturnGoods(coId, orderStatus);
        }

        private void BindOrderStatusDdlList(OrderStatus orderStatusId)
        {
            ViewBag.OrderStatusDdlList = iCustomerOrdersBLL.GetActiveOrderStatusDdlList((uint)(orderStatusId));
        }

        [HttpPost]
        public ActionResult UpdateReturnGoods(string[] returnProduct, CustomerOrdersViewModels customerOrderVM)
        {
            string completeUrl = Request.UrlReferrer.AbsolutePath.ToString();
            string[] arrayUrl = new string[4];
            arrayUrl = completeUrl.Split('/');
            if (returnProduct != null)
            {
                iCustomerOrdersBLL.ChangeCurrentOrderStatusofReturnProducts(returnProduct);
            }
            return RedirectToAction(arrayUrl[3], new { coId = customerOrderVM.Id}); // Redirect to current page
            //return RedirectToAction(arrayUrl[3] + "s");  // Redirect to last request
        }

        //Change Status of Current Order 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeCurrentOrderStatus(CustomerOrdersViewModels customerOrderVM, string CodPaymentMode)
        {
            // Array for Complete Product Path
 
            string completeUrl = Request.UrlReferrer.AbsolutePath.ToString();
            string[] arrayUrl = new string[4];
            arrayUrl = completeUrl.Split('/');
            if (ModelState.IsValid)
            {
                if (iCustomerOrdersBLL.ChangeOrderStatus(customerOrderVM.Id, customerOrderVM.OrderStatusId, SessionManager.CurrentUserId) != 0)
                {
                    if(customerOrderVM.OrderStatusId == (uint)OrderStatus.Delivered)
                    {
                        // Add Loyalty Points
                        iCustomerOrdersBLL.UpdateCustomerLoyaltyPoints(customerOrderVM.Id, (uint)OrderStatus.Delivered);
                        // Update Cash On Delivery Payment Mode
                        if(CodPaymentMode != null || !CodPaymentMode.Equals(CashOnDeliveryPaymentMode.NA))
                        {
                            iCustomerOrdersBLL.UpdateCustomerOrderCodPaymentMode(customerOrderVM.Id, CodPaymentMode);
                        }
                    }
                    
                    // Restore Product Stock
                    if (customerOrderVM.OrderStatusId == (uint)OrderStatus.Cancelled)
                    {
                        iCustomerOrdersBLL.RevertProductsStock(customerOrderVM.Id, (uint)OrderStatus.Cancelled, SessionManager.CurrentUserId);
                    }

                    return RedirectToAction(arrayUrl[3] + "s");  // Redirect to last request
                }
                else
                {
                    TempData["Message"] = "Order Status not Updated successfully, Please try again!";
                    TempData["CssClass"] = "text-success";
                    return RedirectToAction(arrayUrl[3]); // Redirect to current page
                }
            }
            return View(arrayUrl[3]);
        }

        //[HttpPost]
        public ActionResult PrintOrder(ulong orderId = 0, ulong orderStatus = 0)
        {
            CustomerOrdersViewModels customerOrderModelView = null;
            if(orderId != 0)
                customerOrderModelView = iCustomerOrdersBLL.GetCustomerOrderToPrint(orderId, orderStatus, SessionManager.CurrentUserId);
            return View(customerOrderModelView);
        }

        public string RenderRazorViewToString(ulong orderId)
        {
            CustomerOrdersViewModels customerOrderModelView = GetCustomerOrder(OrderStatus.ReadyToDispatch, orderId);
            ViewData["certificate"] = customerOrderModelView;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, "PrintOrder");
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);

                return sw.GetStringBuilder().ToString();
            }
        }

        //public int ChangeCurrentOrderStatusofReturnProducts(ulong prodId = 0, ulong coId = 0, ulong spId = 0)
        //{
        //    if(prodId != 0 && coId != 0 && spId != 0)
        //    {
        //        if(iCustomerOrdersBLL.ChangeCurrentOrderStatusofReturnProducts(prodId, coId, spId) == 0)
        //        {
        //            return 1;
        //        }
        //    }
        //    return 0;
        //}

        

    }
}