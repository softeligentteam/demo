﻿using DailyBasket.Franchisee.Core.Interface.BLL.ProductMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyBasket.Franchisee.Core.Model.Product;
using System.Web.Mvc;
using DailyBasket.Franchisee.Repository.ProductMaster;
using System.Data;
using DailyBasket.Franchisee.Helper;
using DailyBasket.Franchisee.Core.Model.DropDownList;
using System.Collections;
using DailyBasket.Franchisee.Core.Enum;
using System.Runtime.CompilerServices;

namespace DailyBasket.Franchisee.Areas.ProductMaster.BLL
{
    public class ProductBLL : IProduct
    {
        Core.Interface.DAL.ProductMaster.IProduct iproductRepository;
        public ProductBLL()
        {
            iproductRepository = new ProductRepository();
        }
    
        public SelectList GetActiveProductListDdl()
        {
            return DropdownlistHelper.GetLongStringDDList(iproductRepository.GetActiveProductListDdl());
        }

        public SelectList GetActiveProdListDdl()
        {
            return DropdownlistHelper.GetLongStringDDList(iproductRepository.GetActiveProductListDdl());
        }

        public SelectList GetActiveProductListDdl(uint mainCategoryId, uint subCategoryId)
        {
            return DropdownlistHelper.GetLongStringDropDownList(iproductRepository.GetActiveProductListDdl(mainCategoryId, subCategoryId));
        }

     
        public List<ProductModel> GetProductList()
        {
            DataTable dataTable = iproductRepository.GetProductList();
            if (dataTable != null)
            {
                List<ProductModel> productList = new List<ProductModel>();
                foreach (DataRow row in dataTable.Rows)
                {
                    ProductModel productModel = new ProductModel();
                    productModel.Id = Convert.ToUInt32(row["prod_id"].ToString());
                    productModel.Name = row["prod_name"].ToString();
                    productModel.SelectProductHSNNumberId = row["hsn_number"].ToString();
                    productModel.SelectMainCategoryId = row["main_category"].ToString();
                    productModel.SelectSubCategoryId = row["sub_category"].ToString();
                    productModel.Status = Convert.ToByte(row["status"].ToString());

                    productList.Add(productModel);
                }
                return productList;
            }
            return null;
        }
        
    }
}