﻿using DailyBasket.Franchisee.Core.Interface.BLL.ProductMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DailyBasket.Franchisee.Core.Model.Product;
using DailyBasket.Franchisee.Repository.ProductMaster;
using System.Data;
using System.Web.Mvc;
using DailyBasket.Franchisee.Core.Model.DropDownList;
using DailyBasket.Franchisee.Helper;

namespace DailyBasket.Warehouse.Areas.ProductMaster.BLL
{
    public class ProductCategoryBLL : IProductCategory
    {
        ProductCategoryRepository productCategoryRepository = new ProductCategoryRepository();
        // Add Product's Main Category
        

        // Add Product's Sub Category
        

        public SelectList GetAllSubCatogoryListDdl()
        {
            return DropdownlistHelper.GetIntStringDropDownList(productCategoryRepository.GetAllSubCatogoryListDdl());
        }
    }
}