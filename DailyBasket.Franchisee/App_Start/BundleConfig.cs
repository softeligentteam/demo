﻿using System.Web.Optimization;

namespace DailyBasket.Franchisee.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Template CSS Bundles
            bundles.Add(new StyleBundle("~/bundles/css/layout").Include(
                        "~/contents/Skins/_all-skins.min.css",
                        "~/contents/blue.css",
                        "~/contents/custom.css"));

            bundles.Add(new StyleBundle("~/bundles/css/bootstrap").Include(
                      "~/contents/bootstrap/bootstrap.min.css",
                      "~/contents/date-picker/jquery-ui.css"));

            bundles.Add(new StyleBundle("~/bundles/css/date-picker").Include(
                      "~/contents/date-picker/jquery-ui.css"));

            bundles.Add(new StyleBundle("~/bundles/css/dailybasket").Include(
                      "~/contents/dailybasket.min.css"));

            // Template JS Bundles
            bundles.Add(new ScriptBundle("~/bundles/script/jquery").Include(
                        "~/scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/script/layout").Include(
                     "~/scripts/app.min.js",
                      "~/scripts/pages/dashboard.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/bootstrap-ui").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/datepicker/jquery.inputmask.date.extensions.js",
                      "~/scripts/datepicker/jquery-inputmask.js",
                      "~/scripts/datepicker/date-picker.js",
                      "~/scripts/jquery-ui.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/bootstrap").Include(
                      "~/scripts/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/moment").Include(
                      "~/scripts/raphael-min.js",
                      "~/scripts/moment.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/jQuery-2.2.0").Include(
                      "~/scripts/jQuery-2.2.0.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/jquery-ui").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/jquery-ui.js"));

            //Asynchronous Requests JS
            bundles.Add(new ScriptBundle("~/bundles/script/async/Purchase/purchase-sale-entry-validation").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/AsyncRequests/ProductMaster/product-autopopulate-list.js",
                      "~/scripts/AsyncRequests/Purchase/purchase-sale-entry-validation.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/Purchase/pop-validatations-form").Include(
                     "~/scripts/AsyncRequests/ProductMaster/product-sale-rate.js",
                     "~/scripts/AsyncRequests/Stock/product-batch-validation.js",
                     "~/scripts/AsyncRequests/Purchase/pop-validatations-form.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/productmaster/product-autopopulate-list-with-jquery").Include(
                      "~/scripts/jQuery-2.2.0.min.js",
                      "~/scripts/AsyncRequests/ProductMaster/product-autopopulate-list.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/async/purchase/pop-delete-validation-form").Include(
                   "~/scripts/jQuery-2.2.0.min.js",
                   "~/scripts/AsyncRequests/Purchase/pop-delete-validation-form.js"));

            bundles.Add(new ScriptBundle("~/bundles/script/sale/print-customer-orders").Include(
                "~/scripts/jQuery-2.2.0.min.js",
                "~/scripts/AsyncRequests/Sale/print-customer-order.js"));
        }
    }
}