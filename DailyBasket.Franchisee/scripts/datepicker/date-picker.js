﻿$(function () {
    //$.datepicker.setDefaults($.datepicker.regional["es"]);
    
    $(".datepicker").datepicker({
          shortYearCutoff: 1,
          changeMonth: true,
          changeYear: true,
          yearRange: "1950:+10",
          dateFormat: 'dd-mm-yy'
    });

    $(".datepicker").attr("data-mask", true);
    $("#datemask").inputmask("dd-mm-yyyy", { "placeholder": "dd-mm-yyyy" });
    $("[data-mask]").inputmask();
  });



