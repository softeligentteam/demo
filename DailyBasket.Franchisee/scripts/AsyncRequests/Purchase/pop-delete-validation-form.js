﻿function deleteProductEntry(poid, prodid) {
    debugger;
    var poId = poid;
    var prodId = prodid;
    
    if (confirm("Are you sure to delete this Product?")) {
        if (poId !== '') {
            $.get("/WebApi/Purchase/DeleteProductEntry", { 'poId': poId, 'prodId': prodId })
            .done(function (data) {
                if (data !== 0) {
                    alert('Product is deleted successfully!');
                    $.get("/WebApi/Purchase/DeleteProductEntry", { 'poId': poId })
                    .done(function (data) {
                        $("#reloadsection").load(location.href + " #reloadsection");
                    });
                }
                else {
                    alert('Sorry, Product is not deleted successfully, Please try again!');
                }
            });
        }
        else {
            alert('Sorry no data found!');
        }
    }
    else {
        var status = false;
        return false;
    }
}