﻿var autoPopulateProductList = new Array();

$(document).ready(function () {
    debugger;
    //jsonProductList =
    $(jsonProductList).each(function (index, product) {
        autoPopulateProductList.push(product.Text);
    });

    if ($("#SelectProductId").val() != '') {
        var id = "";
        $(jsonProductList).each(function (index, product) {
            if (product.Text == $("#SelectProductId").val()) {
                id = product.Value;
            }
        });
        $('#ProductId option:selected').removeAttr('selected');
        $("#ProductId option[value='" + id + "']").attr("selected", true);
        $('#ProductId').val(id);
    }

    $("#SelectProductId").autocomplete({
        source: autoPopulateProductList,

        change: function (event, ui) {
            
            if (!ui.item) {
                this.value = '';
                $('#ProductId option:selected').removeAttr('selected');
                $("#ProductId option[value='']").attr("selected", true);
            }
            else {
                var id = "";
                $(jsonProductList).each(function (index, product) {
                    if (product.Text == ui.item.value) {
                        id = product.Value;
                    }
                });
                $('#ProductId option:selected').removeAttr('selected');
                $("#ProductId option[value='" + id + "']").attr("selected", true);
                $('#ProductId').val(id);
                if (isSaleRateExists) {
                    getProductSaleRate();
                }
            }
        }
    });
});
