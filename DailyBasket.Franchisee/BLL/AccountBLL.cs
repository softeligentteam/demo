﻿using DailyBasket.Franchisee.Core.Interface.BLL.Login;
using System;
using DailyBasket.Franchisee.Core.Model.Login;
using DailyBasket.Franchisee.Repository.User;
using System.Data;
using DailyBasket.Franchisee.Core.Model.Users;
using DailyBasket.Franchisee.Core.Helper;

namespace DailyBasket.Franchisee.BLL
{
    public class AccountBLL : IUser
    {
        DailyBasket.Franchisee.Core.Interface.DAL.Login.IUser iUserRepository;
        public AccountBLL()
        {
            iUserRepository = new UserRepository();
        }
        public SessionUserModel Authenticate(LoginModel loginModel)
        {
            loginModel.Password = CryptographyHelper.Encrypt(loginModel.Password);
            DataTable dataTable = iUserRepository.Authenticate(loginModel);
            if(dataTable != null && dataTable.Rows.Count != 0)
            {
                //loginModel = null;
                SessionUserModel userModel = new SessionUserModel();
                userModel.FranchiseeId = Convert.ToUInt64(dataTable.Rows[0]["fuser_id"].ToString());
                userModel.UserName = dataTable.Rows[0]["owner_name"].ToString();
                userModel.UserType = dataTable.Rows[0]["user_type"].ToString();
                userModel.FranchiseeName = dataTable.Rows[0]["franchisee_name"].ToString();
                return userModel;
            }
            return null;
        }
    }
}